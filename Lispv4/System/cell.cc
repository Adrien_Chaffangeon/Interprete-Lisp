#include <iostream>
#include <cassert>
#include <cstring> // For strdup
#include <iomanip>

#include "cell.hh"

using namespace std;

/*The two following Objects are iniatilized at the begining of the wolrd*/
Cell* Cell::Object_nil = make_cell_symbol("nil");
Cell* Cell::Object_t = make_cell_symbol("t");

/*Definition of Cell constructor, by default the cell is UNDEFINED*/
Cell::Cell() {
  sort = UNDEFINED;
  value.as_string = NULL;
  index = 0;
  clog << "   New Cell"<<endl;
}

/*Translates sort k into a string*/
string Cell::sort_to_string(cell_sort k) {
  switch(k) {
  case UNDEFINED:
    return "UNDEFINED";
    break;
  case NUMBER:
    return "NUMBER";
    break;
  case STRING:
    return "STRING";
    break;
  case SYMBOL:
    return "SYMBOL";
    break;
  case PAIR:
    return "PAIR";
    break;
  default:
    cout<<"Erreur Sort"<<endl;
    return "";
  }
}

/*Can be used to init Objects Object_nil et Object_t*/
void Cell::init_Object() {
  Cell::Object_nil = make_cell_symbol("nil");
  Cell::Object_t = make_cell_symbol("t");
}

/*Prints the type of the Cell*/
void Cell::check() const {
  cout << "La cellule est de type : " << sort_to_string(sort) << endl;
}

/*Returns the sort of the Cell*/
Cell::cell_sort Cell::get_sort() const {
  return sort;
}

/**Tests on the sort of the cell**/
bool Cell::is_number() const {
  return get_sort() == NUMBER;
}

bool Cell::is_string() const {
  return get_sort() == STRING;
}

bool Cell::is_symbol() const {
  return get_sort() == SYMBOL;
}

bool Cell::is_pair() const {
  return get_sort() == PAIR;
}

/**Translation of Cell into another type**/
int Cell::to_number() const {
  switch(sort) {
  case NUMBER:
    return value.as_number;
    break;
  default:
    cout << "Demande un nombre à une cellule non-nombre" << endl;
    return 0;
  }
}

string Cell::to_string() const {
  switch(sort) {
  case STRING:
    return value.as_string;
    break;
  case SYMBOL:
    return value.as_symbol;
    break;
  default:
    cout << "Demande un string à une cellule non-string et non-symbol" << endl;
    return "";
  }
}

Cell* Cell::to_pair_item() const {
  switch(sort) {
  case PAIR:
    return value.as_pair.item;
    break;
  default:
    cout << "Demande acces à une paire à une cellule non-pair" << endl;
    return Cell::Object_nil;
  }
}


Cell* Cell::to_pair_next() const {
  switch(sort) {
  case PAIR:
    return value.as_pair.next;
    break;
  default:
    cout << "Demande acces à une paire à une cellule non-pair" << endl;
    return Object_nil;
  }
}

/**Creations of Cell**/
Cell* Cell::make_cell_number(int a) {
  Cell* c = new Cell();
  c->sort = NUMBER;
  c->value.as_number = a;
  cout << "    \033[33mCell " << sort_to_string(c->sort) << " Value " << a << " Adress " << (void*) c << "\033[37m" << endl;
  return c;
}

Cell* Cell::make_cell_string(string s) {
  Cell* c = new Cell();
  c->sort = STRING;
  c->value.as_string = strdup(s.c_str());
  cout << "    \033[33mCell " << sort_to_string(c->sort) << " Value " << s << " Adress " << (void*) c << "\033[37m" << endl;
  return c;
}

Cell* Cell::make_cell_symbol(string s) {
  Cell* c = new Cell();
  c->sort = SYMBOL;
  c->value.as_string = strdup(s.c_str());
  cout << "    \033[33mCell " << sort_to_string(c->sort) << " Value " << s << " Adress " << (void*) c << "\033[37m" << endl;
  return c;
}

Cell* Cell::make_cell_pair(Cell* const p, Cell* const q) {
  Cell* c = new Cell();
  c->sort = PAIR;
  c->value.as_pair.item = p;
  c->value.as_pair.next = q;
  cout << "    \033[33mCell Pair " << "Adress " << (void*) c << "\033[37m" << endl;
  return c; 
}

/**Overload of <<**/
ostream& operator <<(ostream& s, const Cell& c) {
  s << "Value : ";
  if(c.is_pair()) {
    s << "(Item : " << *(c.to_pair_item()) << endl << "  Next : " << *(c.to_pair_next()) << ")";
  } else {
    if(c.is_number()) {
      s << c.to_number();
    } else {
      if(c.is_string()) {
	s << c.to_string();
      } else {
	if(c.is_symbol()) {
	  s << c.to_string();
	}else {
	  s << "undefined";
	}
      }
    }
  } 
  return s << endl;
}
