#include <iostream>
#include <cassert>
#include <string>

#include "env.hh"

using namespace std;

/**Exceptions**/
No_Binding_Exception::No_Binding_Exception(string _name):
  runtime_error(_name) {
  name = _name;
}

No_Binding_Exception::~No_Binding_Exception() throw () {}


Environment make_environment(){
  return nil();
}

void add_new_binding(string name, const Object& value, Environment& env){
  Object first = cons(symbol_to_Object(name), cons(value, nil()));
  env = cons(first, env);
  }

Object find_value(string name, const Environment& env){
  if(null(env)){
    throw No_Binding_Exception(name);
  }else{
    if(eq(caar(env), symbol_to_Object(name))){
      return cadar(env);
    }else{
      return find_value(name, cdr(env));
    }
  }
}

ostream& print_env(ostream& s, const Environment& env){
  return s << env;
}

Object copy_object(const Object& l){
  if(numberp(l)){
    return number_to_Object(Object_to_number(l));
  }
  if(stringp(l)){
    return string_to_Object(Object_to_string(l));
  }
  if(symbolp(l)){
    return symbol_to_Object(Object_to_symbol(l));
  }
  if(listp(l)){
    return cons(copy_object(car(l)), copy_object(cdr(l)));
  }else{
    return nil();
  }
}

Environment copy_env(const Environment& env) {
  if (null(env)) {
    return (nil());
  }
  return cons(copy_object(car(env)), copy_object(cdr(env)));
}



