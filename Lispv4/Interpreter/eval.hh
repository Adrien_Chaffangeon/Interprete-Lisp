#pragma once

#include <stdexcept>

#include "interface.hh"

using namespace std;

class Evaluation_Exception;
class Zipping_Exception;

Object eval(const Object& l, const Environment& env);
Object eval_list(const Object& lvals, const Environment& env);
Object eval_cond(const Object& lclauses, const Environment&env);
Object eval_if(const Object& lvals, const Environment&env);
Object eval_let(const Object& lbindings, const Object& body,const Environment&env);

Object unzip_left(const Object& lbindings);
Object unzip_right(const Object& lbindings);

Environment extend_env(const Object& lpars, const Object& lvals, const Environment& env);

Object apply(const Object& f, const Object& lvals, const Environment& env);
Object apply_symbol(const Object& f, const Object& lvals, const Environment& env);
