#pragma once

#include <stdexcept>

#include "interface.hh"

using namespace std;

class Continue_Exception;

void toplevel(bool use_prompt, Environment& env);
Object extract_defining_expr(const Object& obj);
void handle_directive(const Object& l, const Environment& env);
