#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>

#include "interface.hh"

using namespace std;

/*define in Memory.hh*/
class Memory;

/**
Return an empty environment
\return : returns an Environment init with nil()
*/
Environment make_environment();

/**
Create a link between a string and an Object in an environment
\param the name of the symbol, the value that will be link with name, the environment
\return : add a pair, containing name and value, to the Environment env
*/
void add_new_binding(string name, const Object& value, Environment& env);

/**
Return the Object associate to a name in an environment
\param name of the variable and the environment
\return returns the Object associate to name in env, if name is in env. Otherwise it raises a Binding_Exception
*/
Object find_value(string name, const Environment& env);

/**
Give the the time of the newest pair, in env, that contains the variable name
\param name of the variable and the environment
\return an int that represent when, for the last time, the variable has been had to the environment
*/
int find_time_value(string name, const Environment& env);

/**
Give an environment that is a copy of an environment
\param the environment that we want to copy
\return return a copy of the environment env
*/
Environment copy_env(const Environment& env);

/**
Print the environment into a stream
\param the stream where the given environment will be print
\return : return the stream where the environment has been printed
*/
ostream& print_env(ostream& s, const Environment& env);
