#include <iostream>
#include <cassert>
#include <string>

#include "env.hh"

using namespace std;

/**Exceptions**/
No_Binding_Exception::No_Binding_Exception(string _name):
  runtime_error(_name) {
  name = _name;
}

No_Binding_Exception::~No_Binding_Exception() throw () {}

/* Return a fresh env*/
Environment make_environment() {
  return nil();
}

/* Create a link between name and value in env*/
void add_new_binding(string name, const Object& value, Environment& env) {
  static unsigned time = 0;
  Object first = cons(symbol_to_Object(name), cons(value, cons(number_to_Object(time), nil())));
  time ++;
  env = cons(first, env);
}

/* Return the Object associate to name in the environment env*/
Object find_value(string name, const Environment& env){
  if(null(env)){
    throw No_Binding_Exception("Not in the environment: " + name);
  }else{
    if(symbolp(caar(env)) && Object_to_symbol(caar(env)) == name){
      return cadar(env);
    } else {
      return find_value(name, cdr(env));
    }
  }
}

/* Return when name has been had in env for the last time*/
int find_time_value(string name, const Environment& env){
  if(null(env)){
    throw No_Binding_Exception("Not in the environment: " + name);
  }else{
    if(symbolp(caar(env)) && Object_to_symbol(caar(env)) == name){
      return Object_to_number(caddar(env));
    } else {
      return find_time_value(name, cdr(env));
    }
  }
}

/*Print the environment env into the stream s*/
ostream& print_env(ostream& s, const Environment& env) {
  if (null(env)) {
    return s << " ()";
  }
  s << " (" << caar(env) << ", " << cadar(env) << ", " << caddar(env) << ")";
  print_env(s, cdr(env));
  return s;
}

/* Return a copy of the object l*/
Object copy_object(const Object& l) {
  if(numberp(l)) {
    return number_to_Object(Object_to_number(l));
  }
  if(stringp(l)) {
    return string_to_Object(Object_to_string(l));
  }
  if(symbolp(l)) {
    return symbol_to_Object(Object_to_symbol(l));
  }
  if(listp(l)) {
    return cons(copy_object(car(l)), copy_object(cdr(l)));
  } else {
    return nil();
  }
}

/* Return a copy of the Environment env*/
Environment copy_env(const Environment& env) {
  if (null(env)) {
    return (nil());
  }
  return cons(copy_object(car(env)), copy_object(cdr(env)));
}
