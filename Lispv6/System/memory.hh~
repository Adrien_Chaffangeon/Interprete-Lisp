#pragma once

#include <vector>

#include "interface.hh"

using namespace std;

class Cell;

class Memory {
private:
  static unsigned size;
  typedef vector<Cell> memory_type;
  memory_type contents;

  enum mark_type {MARKED, UNMARKED};
  typedef vector<mark_type> information_type;
  information_type marks;

  static vector<unsigned> relocation;

  Memory(unsigned size);

  static Memory global_memory;

  static void mark_cell(Cell* p);
  static void mark_cells(const Environment& env);

  static void clear_cell(Cell& c);
  static void free_cells();

  static void relocate_cells();
  static void adjust_cells();
  static void check_cells();

  static Cell* new_address(Cell* p);

public:
  static Cell* allocate();
  static void dump();
  static void collect(Environment& env);
};

class Memory_Overflow_Exception;
