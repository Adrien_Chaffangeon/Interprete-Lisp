#include <cstdio>

#include "subr.hh"
#include "fsubr.hh"
#include "toplevel.hh"

#include "cell.hh"
#include "memory.hh"
#include "interface.hh"

using namespace std;

bool debug_mod = false;

int main() {

  Environment global_env = make_environment();

  try {

    toplevel(true, global_env);

  } catch (runtime_error(e)) {
    clog << e.what() << endl;
    clog << "It's my Lisp!" << endl;
  }
}
