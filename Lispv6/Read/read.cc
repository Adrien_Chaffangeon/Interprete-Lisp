#include "read.hh"
#include "../Interpreter/interface.hh"

using namespace std;

Reader_Exception::Reader_Exception(string _message):
  runtime_error(_message) {
  message = _message;
}

Reader_Exception::~Reader_Exception() throw () {}

bool test_validity(char s){
	bool cond1 = (s == '+' || s == '*' || s == '-' || s == '=');
	bool cond2 = (s == '"' || s == ' ' || s == '\'' || s == '_');
	bool cond3 = (s == '(' || s == ')');
	/* isdigit(s) = true iif s is in [a...z] or  [A...Z]*/
	/* isalpha(s) = true iif s is in [0...9]*/
	bool cond4 = (isdigit(s) || isalpha(s));
	return (cond1 || cond2 || cond3 || cond4);
}

Token::token_sort Token::get_sort() const{
	return tok_sort;
}

string Token::get_value() const{
	return string_value;
}

int Token::get_number() const{
	return number;
}

string Token::get_symbol() const{
	return symbol_value;
}

unsigned Buffer::get_index() const{
	return index;
}

void Buffer::set_index(int n){
	index = n;
}

unsigned Buffer::get_size() const{
	return line.size();
}

char Buffer::look_char(){
	if (index == (line.size()-1)){
		/* We demand for a new line to be enteres only
		 * if we are in the get_char function */
		return (' ');
	}
	else{
		char s = line.at(index+1);
		assert(test_validity(s));
		return s;
	}
}

char Buffer::get_char(bool search_double_quote){
	if (index == (line.size()-1)){
		/* A string in lisp can be written only on one line */
		if (search_double_quote){
			throw Reader_Exception("There is no second double quote");
		}
		index = 0;
		getline(cin,line);
		return line.at(index);
	}
	else{
		index++;
		char s = line.at(index);
		assert(test_validity(s));
		return s;
	}
}

Token get_token(Buffer *buff){
	char a = buff->get_char(false);
	
	/* ' ' is no token */
	if (a == ' '){
		return get_token(buff);
	}
	
	/* Only the osrt of this token matters */
	if (a == '\''){
		Token tok(Token::QUOTE,0,"anything","anything");
		return tok;
	}
	
	/* Read the expression between the two followin ' " '.
	 * Yet, no unvalid char is accepted (from the 
	 * test_validity function) */
	if (a == '"'){
		string s = "";
		char next = buff->get_char(true);
		while(next != '"'){
			s = s + next;
			next = buff->get_char(true);
		}
		Token tok(Token::STRING,0,s,"anything");
		return tok;
	}
	
	/* It is either a token NIL if the following
	 * char is ' ) ', either a token LPAR */
	if (a == '('){
		char next = buff->look_char();
		if (next == ')'){
			buff->get_char(false);
			Token tok(Token::NIL,0,"anything","anything");
			return tok;
		}
		else{
			Token tok(Token::LPAR,0,"anything","anything");
			return tok;
		}
	}
	
	/* It can't be a token NIL, because of what 
	 * is just before */
	if (a == ')'){
		Token tok(Token::RPAR,0,"anything","anything");
		return tok;
	}
	
	/* It is a token SYMBOL */
	if (a == '+' || a == '*' || a == '='){
		char b[0];
		b[0] = a;
		string symbol = b;
		Token tok(Token::SYMBOL,0,"anything",symbol);
		return tok;
	}
	
	/* It is either a token NUMBER if the followin char 
	 * is a number, either just a SYMBOL */
	if (a == '-'){
		char next = buff->look_char();
		if (isdigit(next)){
			char nextint = next;
			int b = nextint;
			while(isdigit(nextint)){
				b = 10 * b + next;
				nextint = buff->get_char(false);
			}
			Token tok(Token::NUMBER,-b,"anything","anything");
			return tok;
		}
		else{
			char b[0];
			b[0] = a;
			string symbol = b;
			Token tok(Token::SYMBOL,0,"anything",symbol);
			return tok;
		}
	}
	
	/* It is a token NUMBER */
	if (isdigit(a)){
		/* This transformation is due to the ASCII code of 
		 * numbers */
		int b = a-48;
		char next = buff->look_char();
		while(isdigit(next)){
			next = buff->get_char(false);
			b = 10 * b + next-48;
			next = buff->look_char();
		}
		Token tok(Token::NUMBER,b,"anything","anything");
		return tok;
	}
	
	/* It is a token SYMBOL */
	if (isalpha(a) || a == '_'){
		char b[0];
		b[0] = a;
		string s = b;
		char next = buff->look_char();
		while(isalpha(next) || next == '_'){
			next = buff->get_char(false);
			s = s + next;
			next = buff->look_char();
		}
		Token tok(Token::SYMBOL,0,"anything",s);
		return tok;
	}
	throw Reader_Exception("illegal entry");
}

/* This function is called only at the very
 * beginning of the lecture, to know or not 
 * the first token is of sort LPAR */
Token look_token(Buffer *buff){
	int save = buff->get_index();
	Token tok = get_token(buff);
	buff->set_index(save);
	return tok;
}

/* The LPAR token are not directly transformed 
 * into object, just as the QUOTE token */
Object token_to_obj(Token tok){
	if (tok.get_sort() == Token::NUMBER){
		return (number_to_Object(tok.get_number()));
	}
	if (tok.get_sort() == Token::STRING){
		return (string_to_Object(tok.get_value()));
	}
	if (tok.get_sort() == Token::SYMBOL){
		return (symbol_to_Object(tok.get_symbol()));
	}
	if (tok.get_sort() == Token::NIL){
		return (nil());
	}
	if (tok.get_sort() == Token::RPAR){
		return (nil());
	}
	return (nil());
}

/* Read the all expression and test whether or not
 * the token is of sort LPAR.
 * If it is, the expression evaluated stops when 
 * the first ' ( ' is closed, whatever there is after.
 * If not, only one token is evaluated (including 
 * QUOTE token) */
Object get_exp(Buffer *buff){
	Token tok = look_token(buff);
	if (tok.get_sort() == Token::LPAR){
		return (get_expr(buff,true));
	}
	if (tok.get_sort() == Token::QUOTE){
		tok = get_token(buff);
		Object obj1 = symbol_to_Object("quote");
		Object obj2 = get_exp(buff);
		return (cons(obj1,cons(obj2,nil())));
	}
	Object obj = token_to_obj(get_token(buff));
	return (obj);
}

/* Is called when the first token is of sort LPAR.
 * It works recursively on the expression.*/
Object get_expr(Buffer *buff,bool first){
	Token tok = get_token(buff);
	
	/* For the first LPAR token not to be read twice
	 * (in the exp function, only a look_token is used,
	 * not a get_token) */
	if (first == true) {
		return (get_expr(buff,false));
	}
	
	/* If there is a LPAR token, we first read 
	 * the expression until the ' ( ' is closed
	 * (obj1), and then we read the rest of the 
	 * expression (obj2). */
	if (tok.get_sort() == Token::LPAR){
		Object obj1 = get_expr(buff,false);
		Object obj2 = get_expr(buff,false);
		return (cons(obj1,obj2));
	}
	
	/* If there is a QUOTE token, we first construct
	 * the object that fits what is between the two 
	 * ' " ' (obj3). And then we add what remains 
	 * in the expression (obj4). */
	if (tok.get_sort() == Token::QUOTE){
		Object obj1 = symbol_to_Object("quote");
		Object obj2 = get_exp(buff);
		Object obj3 = cons(obj1,cons(obj2,nil()));
		Object obj4 = get_expr(buff,false);
		return (cons(obj3,obj4));
	}
	
	/* We have to be carefu ll for the NIL token
	 * not to be considered as a LPAR token 
	 * (that's why it does not return nil()) */
	if (tok.get_sort() == Token::NIL){
		Object obj1 = get_expr(buff,false);
		return (cons(nil(),obj1));
	}
	
	/* Allows a cons between an object and the token 
	 * NIL */
	if (tok.get_sort() == Token::RPAR){
		return (nil());
	}
	
	/* a represents the rest of the expression,
	 * after the token tok */
	Object a = get_expr(buff,false);
	return (cons(token_to_obj(tok),a));
}

Object read_object(){
	Buffer a;
	Object b = get_exp(&a);
	return b;
}

