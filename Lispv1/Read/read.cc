#include "read.hh"

#include "../Interpreter/interface.hh"

using namespace std;

Object read_object(){
	static int counter = 0;
	Object n = number_to_Object(counter);
	Object m = number_to_Object(counter+1);
	Object plus = symbol_to_Object("+");
	Object op = cons(plus, cons(n,cons(m, nil())));
	
	counter++;
	return op;
	}
