#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>

#include "interface.hh"

using namespace std;

class Memory;

Environment make_environment();

void add_new_binding(string name, const Object& value, Environment& env);
Object find_value(string name, const Environment& env);

Environment copy_env(const Environment& env);

ostream& print_env(ostream& s, const Environment& env);
