.SUFFIXES:
.SUFFIXES: .cc .hh .o .c .h .orig .d .lex .y

.PHONY: clean all astyle syntax listing geany emacs atom list

CC_FILES	:= $(wildcard [^_]*/*.cc)
HH_FILES	:= $(wildcard [^_]*/*.hh)
O_FILES		:= $(CC_FILES:%.cc=%.o)

GCC_FLAGS	:= -g -Wall -IRead -IInterpreter -ISystem

all: lisp

include dependencies.d

$(O_FILES): %.o: %.cc Makefile
	g++ $(GCC_FLAGS) $< -c -o $@

lisp: $(O_FILES) Makefile
	g++ $(GCC_FLAGS) $(O_FILES) -o $@

ASTYLE_OPTIONS	= --style=attach --indent=spaces=2

astyle:
	astyle $(ASTYLE_OPTIONS) $(CC_FILES) $(HH_FILES)

clean:
	-rm $(O_FILES)
	-rm dependencies.d
	-rm -r *.dSYM html latex
	-rm */*.orig
	rm *~ *#
	-rm lisp

LIST_FILES	= $(sort $(CC_FILES) $(HH_FILES)) Makefile

list:
	@echo $(LIST_FILES)

ENSCRIPT_OPTIONS	= --language=PostScript --missing-characters \
			--borders --nup=2 --word-wrap --mark-wrapped=arrow

listing: astyle
	enscript $(ENSCRIPT_OPTIONS) $(LIST_FILES)  -o tmp.ps; \
	pstopdf tmp.ps -o listing.pdf; rm tmp.ps

emacs geany: astyle clean
	$@ $(LIST_FILES)  &

atom: astyle clean
	$@ .  &

dependencies.d: $(CC_FILES) $(HH_FILES)
	@echo "Computing dependencies..."
	g++ -MM $(GCC_FLAGS) $(CC_FILES) $(HH_FILES) > $@
