#pragma once

#include <iostream>
#include <vector>

#include "memory.hh"

#include "interface.hh"

using namespace std;

class Memory;

class Cell {
private:
  /*Its represents the type of the Cell*/
  enum cell_sort {UNDEFINED, NUMBER, STRING, SYMBOL, PAIR};
  cell_sort sort;

  /*This is kind of Cell that can contain two cell*/
  struct cell_pair {
    Cell* item;
    Cell* next;
  };

  /*Its represents the value of the Cell. There 4 possibles types for this value : Int, String,
  Symbol, cell_pair*/
  union cell_value {
    int as_number;
    char *as_string;
    char *as_symbol;
    cell_pair as_pair;
  };

  cell_value value;

  unsigned index;

private:
  /*Constructor of the Type*/
  Cell(); // No direct allocation
  /*string sort_to_string(cell_sort k): cell_sort -> sort_to_string
   k : cell_sort
   return : */
  static string sort_to_string(cell_sort k);

public:
  /*This object represents an Empty Cell with the logic value False. Its a Symbol Cell*/
  static Cell* Object_nil; //make_cell_symbol("nil")
  /*This object represents a Cell with the logic value True. Its a Symbol Cell*/
  static Cell* Object_t; //make_cell_symbol("t")

public:
  /*check() : void -> void
    return : prints the sort of the Cell*/
  void check() const;

  /*get_sort() : void -> cell_sort
    return : the sort of the Cell*/
  cell_sort get_sort() const;

  /*is_number() : void -> bool
    return : true if the Cell sort is NUMBER, false otherwise*/
  bool is_number() const;

  /*is_string() : void -> bool
    return : true if the Cell sort is STRING, false otherwise*/
  bool is_string() const;

  /*is_symbol() : void -> bool
    return : true if the Cell sort is SYMBOL, false otherwise*/
  bool is_symbol() const;

  /*is_pair() : void -> bool
    return : true if the Cell sort is PAIR, false otherwise*/
  bool is_pair() const;

  /*to_number : void -> int
    return : returns the Int value of the Cell, if the Cell sort is NUMBER*/
  int to_number() const;

  /*to_string : void -> string
    return : returns the String value of the Cell, if the Cell sort is STRING*/
  string to_string() const;

  /*to_number : void -> Cell*
    return : returns the first item of the value of the Cell, if the Cell sort is PAIR*/
  Cell* to_pair_item() const;

  /*to_number : void -> Cell*
    return : returns the second item of the value of the Cell, if the Cell sort is PAIR*/
  Cell* to_pair_next() const;


  /*make_cell_number : int -> Cell*
    Int a : this is the number that the Cell will contain
    return : pointer to a Cell NUMBER that contain a*/
  static Cell* make_cell_number(int a);

  /*make_cell_string : string -> Cell*
    String s : this is the string that the Cell will contain
    return : pointer to a Cell STRING that contain s*/
  static Cell* make_cell_string(string s);

  /*make_cell_symbol : string -> Cell*
    String s : this is the symbol, represents by a string, that the Cell will contain
    return : pointer to a Cell SYMBOL that contain s*/
  static Cell* make_cell_symbol(string s);

  /*make_cell_number : (Cell* x Cell*) -> Cell*
    Cell* p : this is the pointer to a Cell p that the Cell will contain in ITEM
    Cell* q : this is the pointer to a Cell q that the Cell will contain in NEXT
    return : pointer to a Cell PAIR that contain p in ITEM and q in NEXT*/
  static Cell* make_cell_pair(Cell* const p, Cell* const q);

  /*init_Object() : void -> void
    return : reset the value of Object_nil and Object_t to there original value*/
  static void init_Object();

  friend void print_pair(const Cell& c);

  /*ostream& operator <<(ostream& s, const Cell& c) : (ostream& x Cell&) -> ostream&
    ostream& s : this is the stream where c will be put in
    Cell& c : this is the Cell that we will give to the stream
    return : gives the value of c to the stream s*/
  friend ostream& operator <<(ostream& s, const Cell& c);
  
  friend Memory;
  friend vector<Cell>;
  friend void dump();
};

void print_pair(const Cell& c);

/*ostream& operator <<(ostream& s, const Cell& c) : (ostream& x Cell&) -> ostream&
  ostream& s : this is the stream where c will be put in
  Cell& c : this is the Cell that we will give to the stream
  return : gives the value of c to the stream s*/
ostream& operator <<(ostream& s, const Cell& c);
