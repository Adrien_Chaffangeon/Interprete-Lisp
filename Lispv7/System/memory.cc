#include <iostream>
#include <cassert>
#include <vector>
#include <stdexcept>
#include <iomanip>
#include <cstdio>

#include "cell.hh"
#include "memory.hh"

using namespace std;

/*Exception*/
Memory_Overflow_Exception::Memory_Overflow_Exception(string _message):
  runtime_error(_message){
  message = _message;
}
Memory_Overflow_Exception::~Memory_Overflow_Exception() throw () {}

Memory::Memory(unsigned _size) {
  size = _size;
  contents = vector<Cell>(size);
  marks = vector<mark_type>(size);
  for(unsigned i = 0; i < size; i++) {
    contents[i].index = i;
    marks[i] = MARKED;
  }
  Cell::init_Object();
}

Memory Memory::global_memory(50);

/* Reset a cell*/
void Memory::clear_cell(Cell& c) {
  if (c.is_pair()){
      global_memory.contents[c.index].sort = Cell::UNDEFINED;
      global_memory.marks[c.index] = MARKED;
  }else {
    global_memory.contents[c.index].sort = Cell::UNDEFINED;
    global_memory.marks[c.index] = MARKED;
  }
}

/* Give a pointer to an available cell*/
Cell* Memory::allocate() {
  unsigned i = 0;
  while(global_memory.contents[i].sort != Cell::UNDEFINED) {
    i++;
    if (i >= global_memory.size) {
      throw Memory_Overflow_Exception("Pas de place");
    }
  }
  return &global_memory.contents[i];
}

/*Unmark every cell in the memory*/
void Memory::unmark_all() {
  cout << endl << "1" << endl;
  for (unsigned i = 2; i < global_memory.size; i++) {
    global_memory.marks[i] = UNMARKED;
  }
}

/* Mark the given cell*/
void Memory::mark_cell(Cell* p){
  global_memory.marks[p->index] = MARKED;
}

/* Mark the given cell p and every cell that p contains*/
void Memory::mark_rec_cell(Cell* p){
  if (p->is_pair()){
      mark_cell(p);
      mark_rec_cell(p->to_pair_item());
      mark_rec_cell(p->to_pair_next());
  }else {
    mark_cell(p);
  }
}

/* Mark the cell of an environment, if they are still reachable with find_value*/
void Memory::mark_env_cells(const Environment& env, Environment& original){
  if(null(env)) {
    mark_cell(env);
  }
  else {
    if(symbolp(caar(env)) && 
      Object_to_number(caddar(env)) == find_time_value(Object_to_symbol(caar(env)), original)) {
      mark_cell(env);//mark env
      mark_cell(car(env));//mark the item 
      mark_cell(caar(env));//mark name
      mark_rec_cell(cdar(env));//mark pair that hold value
      mark_cell(cadar(env));//mark value
      mark_cell(cddar(env));//mark pair that hold time
      mark_cell(caddar(env));//mark time
      mark_cell(cdr(cddar(env)));
    }
    mark_env_cells(cdr(env), original);
  }
}

/* Give a new environment with no unmark cell*/
Environment Memory::reset_env(Environment env) {
  if(null(env)) {
    mark_cell(env);
    return env;
  }
  else {
    cout << endl << env->index << endl;
    if (global_memory.marks[env->index] == UNMARKED) {
      return reset_env(cdr(env));
    } else {
      return cons(car(env), reset_env(cdr(env)));
    }
  }
}

/* Reset every cell that are unmark*/
void Memory::free_cells() {
  for(unsigned i = 0; i < global_memory.size; i++) {
    if (global_memory.marks[i] == UNMARKED){
      clear_cell(global_memory.contents[i]);
    }
  }
}

/* Mark and reset an environment*/
void Memory::collect(Environment& env) {
  unmark_all();
  mark_env_cells(env, env);
  env = reset_env(env);
  unmark_all();;
  mark_rec_cell(env);
}

/*Clean a cell*/
void Memory::free_cell(Cell* c) {
  Memory::clear_cell(*c);
}

/*Translate a mark_type into a string */
string mark_to_string(Memory::mark_type k) {
  if (k == Memory::MARKED) {
    return "M";
  }
  return "U";
}

/*Print the memory*/
void dump() {
  for(unsigned i = 0; i < Memory::global_memory.size; i++) {
    cout << i << mark_to_string(Memory::global_memory.marks[i]) << " : " << Memory::global_memory.contents[i] << endl;
  }
}

/*Public name of Memory::collect*/
void clean_env(Environment& env){
  Memory::collect(env);
}

/*Public name of Memory::defragmentation*/
void defragmentation(){
  Memory::free_cells();
}
