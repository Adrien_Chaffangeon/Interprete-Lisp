#include <iostream>
#include <cassert>
#include <cstring> // For strdup
#include <iomanip>

#include "cell.hh"

using namespace std;

/*The two following Objects are iniatilized at the begining of the wolrd*/
Cell* Cell::Object_nil = NULL;
Cell* Cell::Object_t = NULL;

/*Definition of Cell constructor, by default the cell is UNDEFINED*/
Cell::Cell() {
  sort = UNDEFINED;
  value.as_string = NULL;
  index = 0;
  if (debug_mod) {
    clog << "   New Cell"<<endl;
  }
}

/*Translates sort k into a string*/
string Cell::sort_to_string(cell_sort k) {
  switch(k) {
  case UNDEFINED:
    return "UNDEFINED";
    break;
  case NUMBER:
    return "NUMBER";
    break;
  case STRING:
    return "STRING";
    break;
  case SYMBOL:
    return "SYMBOL";
    break;
  case PAIR:
    return "PAIR";
    break;
  default:
    cout<<"Erreur Sort"<<endl;
    return "";
  }
}

/*Can be used to init Objects Object_nil et Object_t*/
void Cell::init_Object() {
  Cell::Object_nil = make_cell_symbol("nil");
  Cell::Object_t = make_cell_symbol("t");
}

/*Prints the type of the Cell*/
void Cell::check() const {
  cout << "La cellule est de type : " << sort_to_string(sort) << endl;
}

/*Returns the sort of the Cell*/
Cell::cell_sort Cell::get_sort() const {
  return sort;
}

/**Tests on the sort of the cell**/
bool Cell::is_number() const {
  return get_sort() == NUMBER;
}

bool Cell::is_string() const {
  return get_sort() == STRING;
}

bool Cell::is_symbol() const {
  return get_sort() == SYMBOL;
}

bool Cell::is_pair() const {
  return get_sort() == PAIR;
}

/**Translation of Cell into another type**/
int Cell::to_number() const {
  switch(sort) {
  case NUMBER:
    return value.as_number;
    break;
  default:
    cout << "Demande un nombre à une cellule non-nombre" << endl;
    return 0;
  }
}

string Cell::to_string() const {
  switch(sort) {
  case STRING:
    return value.as_string;
    break;
  case SYMBOL:
    return value.as_symbol;
    break;
  default:
    cout << "Demande un string à une cellule non-string et non-symbol" << endl;
    return "";
  }
}

Cell* Cell::to_pair_item() const {
  switch(sort) {
  case PAIR:
    return value.as_pair.item;
    break;
  default:
    cout << "Demande acces à une paire à une cellule non-pair" << endl;
    return Cell::Object_nil;
  }
}


Cell* Cell::to_pair_next() const {
  switch(sort) {
  case PAIR:
    return value.as_pair.next;
    break;
  default:
    cout << "Demande acces à une paire à une cellule non-pair" << endl;
    return Object_nil;
  }
}

/**Creations of Cell**/
Cell* Cell::make_cell_number(int a) {
  Cell* c = Memory::allocate();
  c->sort = NUMBER;
  c->value.as_number = a;
  if (debug_mod) {
  cout << "    \033[33mCell: " << sort_to_string(c->sort) << " (Int: " << a << ") Adress: " << (void*) c << " Index: " << c->index << "\033[37m" << endl;
  }
  return c;
}

Cell* Cell::make_cell_string(string s) {
  Cell* c = Memory::allocate();
  c->sort = STRING;
  c->value.as_string = strdup(s.c_str());
  if (debug_mod) {
  cout << "    \033[33mCell: " << sort_to_string(c->sort) << " (String: " << s << ") Adress: " << (void*) c << " Index: " << c->index << "\033[37m" << endl;
  }
  return c;
}

Cell* Cell::make_cell_symbol(string s) {
  Cell* c = Memory::allocate();
  c->sort = SYMBOL;
  c->value.as_string = strdup(s.c_str());
  if (debug_mod) {
  cout << "    \033[33mCell: " << sort_to_string(c->sort) << " (Symbol: " << s << ") Adress: " << (void*) c << " Index: " << c->index << "\033[37m" << endl;
  }
  return c;
}

Cell* Cell::make_cell_pair(Cell* const p, Cell* const q) {
  Cell* c = Memory::allocate();
  c->sort = PAIR;
  c->value.as_pair.item = p;
  c->value.as_pair.next = q;
  if (debug_mod) {
  cout << "    \033[33mCell: Pair " << "Adress: " << (void*) c << " Index: " << c->index << "\033[37m" << endl;
  }
  return c;
}


/*Print of Pair*/
void print_pair(const Cell& c) {
    if(c.is_pair()) {
      print_pair(*c.to_pair_item());
      cout << ", ";
      print_pair(*c.to_pair_next());
  } else {
    if(c.is_number()) {
      cout << c.to_number();
    } else {
      if(c.is_string()) {
	cout << c.to_string();
      } else {
	if(c.is_symbol()) {
	  cout << c.to_string();
	} else {
	  cout << "Undefined";
	}
      }
    }
  }
}



/**Overload of <<**/
ostream& operator <<(ostream& s, const Cell& c) {
  if(c.is_pair()) {
    s << "List: (";
    print_pair(*c.to_pair_item());
    s << ", ";
    print_pair(*c.to_pair_next());
    s << ")";
  } else {
    if(c.is_number()) {
      s << "(Int: " << c.to_number() << ")";
    } else {
      if(c.is_string()) {
	s << "(String: " << c.to_string() << ")";
      } else {
	if(c.is_symbol()) {
	  s << "(Symbol: " << c.to_string() << ")";
	} else {
	  s << "Undefined";
	}
      }
    }
  }
  return s;
}
