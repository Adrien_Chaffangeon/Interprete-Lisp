#pragma once

#include <vector>

#include "interface.hh"

using namespace std;

class Cell;

class Memory {
private:
  unsigned size;
  typedef vector<Cell> memory_type;
  memory_type contents;

  enum mark_type {MARKED, UNMARKED};
  typedef vector<mark_type> information_type;
  information_type marks;
/*
  static vector<unsigned> relocation;
*/
  Memory(unsigned size);

  static Memory global_memory;

  static void free_cell(Cell* c);
  static void free_cells();

  static void unmark_all();
  static void mark_cell(Cell* p);
  static void mark_rec_cell(Cell* p);
  static void mark_env_cells(const Environment& env, Environment& original);

  static Environment reset_env(Environment env);

  static void clear_cell(Cell& c);

  static void relocate_cells();
  static void adjust_cells();
  static void check_cells();

  static Cell* new_address(Cell* p);

public:
  static Cell* allocate();

  static void collect(Environment& env);
  
  friend void dump();
  friend string mark_to_string(Memory::mark_type k);
  friend void clean_env(Environment& env);
  friend void defragmentation();
};

string mark_to_string(Memory::mark_type k);
void dump();
void clean_env(Environment& env);
void defragmentation();

class Memory_Overflow_Exception;
