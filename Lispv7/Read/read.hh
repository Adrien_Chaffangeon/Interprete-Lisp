#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <stdexcept>
#include <cassert>

#include "../Interpreter/interface.hh"

using namespace std;

/**
* Return whether or not the param is a valid char
 \param A char that we have read on the screen
 \return A boolean which is true iif the param is valid
*/
bool test_validity(char s);

/**
* Return the transformation of a char into string
 \param A char that we have read on the screen
 \return A string
*/
string char_into_string(char s);

class Token {
	public: 
		/* Spaces are no token */
		enum token_sort {NIL,LPAR,RPAR,NUMBER,STRING,SYMBOL,QUOTE};
		
	private:
		token_sort tok_sort;
		int number;
		string string_value;
		string symbol_value;
		
	public:
		Token(token_sort tok,int num,string str,string symbol): tok_sort(tok), number(num),string_value(str),symbol_value(symbol) {}
		
		/**
		Return the kind of the token 
		\param nothing
		\return A tiken_sort which is the sort of the token 
		*/
		token_sort get_sort() const;
		
		/**
		* Return the value of the token, which is irrelevant if the sort
		* of the token is not STRING
		\param nothing
		\return A string shich is the value of the token 
		*/
		string get_value() const;
		
		/**
		* Return the number of the token, which is irrelevant if the sort
		* of the token is not NUMBER
		\param nothing
		\return An integer which is the number of the token 
		*/
		int get_number() const;
		
		/**
		* Return the symbol of the token, which is irrelevant if the sort
		* of the token is not SYMBOL
		\param nothing
		\return A string <hich is the symbol of the token 
		*/
		string get_symbol() const;
};

class Buffer {
	private:
		string line;
		unsigned index;
	
	public:
		Buffer(): line("a"), index(0) {
			getline(cin,line);
			line = ' ' + line;
		}
		
		/**
		* Return the index of the buffer that indicates what char 
		* we will read in the buffer
		\param nothing
		\return An unsigned which is the index of the buffer
		*/
		unsigned get_index() const;
		
		/**
		* Set the index of the buffer to the value n
		\param A integer n which is the new value of the index
		\return Nothing
		*/
		void set_index(int n);
		
		/**
		* Return the size of the buffer that indicates what char 
		\param nothing
		\return An unsigned which is the index of the buffer
		*/
		unsigned get_size() const;
		
		/**
		* Return the next char we are reading in the buffer without
		* increasing the index
		\param nothing
		\return An char which is the next char of the buffer
		*/
		char look_char();
		
		/**
		* Return the next char we are reading in the buffer and 
		* increases the index.
		* There is an error if it is the end of the line, and we are 
		* searching a ' " ' (param search_double_quote)
		\param A boolean search_double_quote that indicates whether 
				or not we are searching for a ' " '
		\return An char which is the next char of the buffer
		*/
		char get_char(bool search_double_quote);
};

/**
* Return the next token we are reading in the expression.
* It increases the index of the buffer to remember that the
* reading of the token has advanced
 \param A pointer on the buffer we are reading
 \return A token which is the next we are reading
*/
Token get_token(Buffer& buff);

/**
* Return the next token we are reading in the expression
* without increasing the index of the buffer
 \param A pointer on the buffer we are reading
 \return A token which is the next we are reading
*/
Token look_token(Buffer& buff);

/**
* Return an object which is the transformation of the 
* given token
 \param A token we want to have as an object
 \return A object that corresponds to the given token
*/
Object token_to_obj(Token tok);

/**
* Return the expression we are reading from the beginning
 \param A pointer on the buffer we are reading
 \return An object which represents what we have read
*/
Object get_exp(Buffer& buff);

/**
* Return the expression we are reading from the first 
* ' ( ' if there is one
 \param A pointer on the buffer we are reading, and 
 \return An object which represents what we have read
*/
Object get_expr(Buffer& buff,bool first);

/**
* Return the object that is to be evaluate
 \param Nothing
 \return An object to evaluate
*/
Object read_object();
