#pragma once

#include <map>
#include <cassert>

#include "interface.hh"

using namespace std;

class Fsubr {
public:
  typedef Object (*fsubr_type)(Object, Environment);

private:
  typedef map<string, fsubr_type> table_type;
  table_type table;

  static Fsubr fsubr_table;

  Fsubr();
  void init();

public:
  static void add(string s, const fsubr_type& f);
  static bool exists(string s);
  static fsubr_type get(string s);
};
