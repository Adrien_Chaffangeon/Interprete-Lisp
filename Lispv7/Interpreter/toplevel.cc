#include <iostream>

#include "eval.hh"
#include "toplevel.hh"

#include "interface.hh"
#include "read.hh"
#include "env.hh"

using namespace std;

Continue_Exception::Continue_Exception(string _message):
  runtime_error(_message) {
  message = _message;
}

Continue_Exception::~Continue_Exception() throw () {}

Object extract_defining_expr(const Object& obj) {
  if (null(cdddr(obj))) {
    cout << caddr(obj) << endl;
    return caddr(obj);
  } else {
    Object rest = cddr(obj);
    return (cons(symbol_to_Object("lambda"), rest));
  }
}

void handle_directive(const Object& l, Environment& env) {
  if (debug_mod) {
    cout << "handle_directive" << endl;
  }
  if (listp(l)) {
    Object directive = car(l);
    if (eq(directive, symbol_to_Object("setq"))) {
      Object obj = cadr(l);
      Object exp = extract_defining_expr(l);
      string name = Object_to_symbol(obj);
      Object value = eval(exp,env);
      clog << "\033[31m#?>SET: \033[37m" << name << " = " << value << endl;
      add_new_binding(name, value, env);
      throw Continue_Exception("");
    }
    if (symbolp(directive) && Object_to_symbol(directive) == "debug_on") {
      debug_mod = true;
      throw Continue_Exception("\033[31m#?>Debug On !!!\033[37m");
    }
    if (symbolp(directive) && Object_to_symbol(directive) == "debug_off") {
      debug_mod = false;
      throw Continue_Exception("\033[31m#?>Debug Off !!!\033[37m");
    }
  }

}

void toplevel(bool use_prompt, Environment& env) {
  int compteur = 1;
  while (compteur) {
    cout <<endl<< "\033[34mLisp? \033[37m" << flush;
    //Object para = cons( symbol_to_Object("n"), nil());
    //Object op = cons( symbol_to_Object("+"), cons( symbol_to_Object("n"), cons(number_to_Object(1), nil())));
    //Object f = cons(symbol_to_Object("lambda"), cons( para, cons(op, nil())));
    //Object n = cons( symbol_to_Object("n"), cons(number_to_Object(2), nil()));

    //Object obj = cons(symbol_to_Object("cond"), cons( cons(nil(), cons(number_to_Object(4), nil())), cons(cons(t(), cons(number_to_Object(5),nil())), nil())));
    // Object obj = cons(symbol_to_Object("null"), cons( cons( symbol_to_Object("quote"),cons( number_to_Object(2), nil())), nil()));
    //"lambda" :: Object obj = cons(f,cons(number_to_Object(2),nil()));
    // Object obj = cons(symbol_to_Object("let"), cons(cons(n,nil()),cons(op,nil())));
    // Object obj = cons(symbol_to_Object("cons"), cons(number_to_Object(2), cons(nil() ,nil())));
    try {
      Object obj = read_object();
      handle_directive(obj,env);
      cout << "\033[31m#?>\033[37m " << eval(obj, env) << endl;
    } catch (No_Binding_Exception& e) {
      clog << e.what() << endl;

    } catch (Continue_Exception e) {
      clog << e.what() << endl;

    } catch (Evaluation_Exception e) {
      clog << e.what() << endl;

    } catch (Zipping_Exception e) {
      clog << e.what() << endl;

    } catch (Reader_Exception e) {
      clog << e.what() << endl;
    }
    /*dump();*/
    clean_env(env);
    defragmentation();
    /*dump();*/
  }
}
