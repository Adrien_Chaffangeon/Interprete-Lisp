/* Bison syntax, left recursion */


main:
list_rpar expr
| Token_EOF
;

expr:
Token_number
| Token_nil
| Token_symbol
| Token_string
| Token_quote expr
| Token_lpar list_expr Token_rpar
;

list_expr:
%empty
| list_expr expr

list_rpar:
%empty
| list_rpar Token_rpar
;
