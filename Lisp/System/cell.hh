#pragma once

#include <iostream>
#include <vector>

#include "memory.hh"

#include "interface.hh"

using namespace std;

class Memory;

class Cell {
private:
  /*Its represents the type of the Cell*/
  enum cell_sort {UNDEFINED, NUMBER, STRING, SYMBOL, PAIR};
  cell_sort sort;

  /*This is kind of Cell that can contain two cell*/
  struct cell_pair {
    Cell* item;
    Cell* next;
  };

  /*Its represents the value of the Cell. There 4 possibles types for this value : Int, String,
  Symbol, cell_pair*/
  union cell_value {
    int as_number;
    char *as_string;
    char *as_symbol;
    cell_pair as_pair;
  };

  cell_value value;

  unsigned index;

private:
  /*Constructor of the Type*/
  Cell(); // No direct allocation
  /**
  Translate a cell_sort into a string
   \param the cell_sort we want to translate
   \return the translation
   */
  static string sort_to_string(cell_sort k);

public:
  /*This object represents an Empty Cell with the logic value False. Its a Symbol Cell*/
  static Cell* Object_nil; //make_cell_symbol("nil")
  /*This object represents a Cell with the logic value True. Its a Symbol Cell*/
  static Cell* Object_t; //make_cell_symbol("t")

public:
  /**
  Print the sort of the Cell
  \param
  \return
  */
  void check() const;

  /**
  Return the sort of the cell
  \param a cell
  \return the sort of the cell
  */
  cell_sort get_sort() const;

  /**
  Test if the Cell sort is NUMBER
  \param a cell
  \return true if the Cell sort is NUMBER, false otherwise
  */
  bool is_number() const;

  /**
  Test if the Cell sort is STRING
  \param a cell
  \return true if the Cell sort is STRING, false otherwise
  */
  bool is_string() const;

  /**
  Test if the Cell sort is SYMBOL
  \param a cell
  \return true if the Cell sort is SYMBOL, false otherwise
  */
  bool is_symbol() const;

  /**
  Test if the Cell sort is PAIR
  \param a cell
  \return true if the Cell sort is PAIR, false otherwise
  */
  bool is_pair() const;

  /**
  Translate a Cell into a number
  \param a cell
  \returns the Int value of the Cell, if the Cell sort is NUMBER
  */
  int to_number() const;

  /**
  Translate a Cell into a string
  \param a cell
  \returns the String value of the Cell, if the Cell sort is STRING
  */
  string to_string() const;

  /**
  Give the first element of a pair
  \param a cell
  \returns the item value of the Cell, if the Cell sort is PAIR
  */
  Cell* to_pair_item() const;

  /**
  Give the first element of a pair
  \param a cell
  \returns the next value of the Cell, if the Cell sort is PAIR
  */
  Cell* to_pair_next() const;


  /**
  Return a pointer to a Cell wich contains a
  \param the number that the Cell will contain
  \return pointer to a Cell NUMBER that contain a
  */
  static Cell* make_cell_number(int a);

  /**
  Return a pointer to a Cell wich contains s
  \param the number that the Cell will contain
  \return pointer to a Cell STRING that contain s
  */
  static Cell* make_cell_string(string s);

  /**
  Return a pointer to a Cell wich contains s
  \param the number that the Cell will contain
  \return pointer to a Cell SYMBOL that contain s
  */
  static Cell* make_cell_symbol(string s);

  /**
  Return a pointer to a Cell wich contains p and q
  \param two cells
  \return pointer to a Cell PAIR that contain p as item and q as next
  */
  static Cell* make_cell_pair(Cell* const p, Cell* const q);

  /**
  reset the value of Object_nil and Object_t to there original value
  \param
  \return
  */
  static void init_Object();

  friend void print_pair(const Cell& c);

  friend ostream& operator <<(ostream& s, const Cell& c);
  
  friend Memory;
  friend vector<Cell>;
  friend void dump();
};

/**
Print a Pair
\param a cell
\return
*/
void print_pair(ostream& s, const Cell& c);

/**
Print a cell into a stream
\param a stream and a cell
\return : gives the value of c to the stream s
*/
ostream& operator <<(ostream& s, const Cell& c);
