#pragma once

#include <vector>

#include "interface.hh"

using namespace std;

/*Define*/
class Cell;

class Memory {
private:
  /*Size of the memory*/
  unsigned size;
  typedef vector<Cell> memory_type;
  memory_type contents;

  /*For the garbage colector, we keep mark cell*/
  enum mark_type {MARKED, UNMARKED};
  typedef vector<mark_type> information_type;
  information_type marks;

  /*Constructor*/
  Memory(unsigned size);

  /*Global memory*/
  static Memory global_memory;

  /**
  Free a given cell
  \param a cell pointer
  \return nothing
  */
  static void free_cell(Cell* c);
  /**
  Free all unmarked cell
  \param 
  \return
  */
  static void free_cells();

  /**
  Unmark every cell
  \param
  \return
  */
  static void unmark_all();
  /**
  Mark a given cell
  \param a cell
  \return
  */
  static void mark_cell(Cell* p);
  /**
  Mark a given cell and every cell it contains
  \param a cell
  \return
  */
  static void mark_rec_cell(Cell* p);
  /**
  Mark every cell of a given environment, if the cell can be reach by find_value
  \param an environment, the two parameters has to be the same
  \return
  */
  static void mark_env_cells(const Environment& env, Environment& original);

  /**
  Create a new environment without unmarked cell
  \param an environment
  \return the new environment without unmarked cell
  */
  static Environment reset_env(Environment env);

  /**
  Clear the given cell
  \param a cell
  \return
  */
  static void clear_cell(Cell& c);

public:
  /**
  Return a pointer to an available Cell
  \param
  \return a pointer to a Cell
  */
  static Cell* allocate();

  /**
  Mark the object we want to keep in the given environment
  \param the environment
  \return
  */
  static void collect(Environment& env);

  friend void dump();
  friend string mark_to_string(Memory::mark_type k);
  friend void clean_env(Environment& env);
  friend void defragmentation();
};

/**
Translate a mark_type into a string
\param
\return
*/
string mark_to_string(Memory::mark_type k);

/**
Print the actual composition of the global_memory
\param
\return
*/
void dump();

/**
Public name of collect()
\param an environment
\return
*/
void clean_env(Environment& env);

/**
Public name of free_cells()
\param
\return
*/
void defragmentation();

/*Exception*/
class Memory_Overflow_Exception;
