#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>

#include "interface.hh"

using namespace std;

class Memory;

/*make_environment() : void -> Environment
  return : returns an Environment init with nil()*/
Environment make_environment();

/*add_new_binding(string name, const Object& value, Environment& env) : (string x Object& x Environment) -> void
  string name : name of the symbol that will be link to value
  const Object& value : value that will be link with name in env
  Environment& env : environment where the link between name and value will be set
  return : add a pair, containing name and value, to the Environment env*/
void add_new_binding(string name, const Object& value, Environment& env);

/*Object find_value(string name, const Environment& env) : (string x Environment&) -> Object
  string name : name of the variable that we want the value
  Environment& env : environment where we want to find name
  return : returns the Object associate to name in env, if name is in env. Otherwise it raises a Binding_Exception*/
Object find_value(string name, const Environment& env);

/**
Give the the time of the newest pair, in env, that contains the variable name
\param name of the variable and the environment
\return an int that represent when, for the last time, the variable has been had to the environment
*/
int find_time_value(string name, const Environment& env);

/*Environment copy_env(const Environment& env) : Environment& -> Environment
  Environment& env : environment that we want to copy
  return : returns a copy of the environment env*/
Environment copy_env(const Environment& env);

/*ostream& print_env(ostream& s, const Environment& env) : (ostream& x Environment&) -> ostream&
  ostream& s : stream where env will be print
  Environment& env : environment that will be printed
  return : return the stream where env has been printed*/
ostream& print_env(ostream& s, const Environment& env);
