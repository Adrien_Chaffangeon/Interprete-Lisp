#include <cassert>

#include "subr.hh"
#include "fsubr.hh"
#include "eval.hh"

#include "interface.hh"

using namespace std;

Evaluation_Exception::Evaluation_Exception(const Object& _obj, const Environment& _env, string _message):
  runtime_error(_message) {
  message = _message;
}

Evaluation_Exception::~Evaluation_Exception() throw () {}

Zipping_Exception::Zipping_Exception(const Object& _lobjs, string _message):
  runtime_error(_message) {
  message = _message;
}

Zipping_Exception::~Zipping_Exception() throw () {}

Object eval(const Object& l, const Environment& env) {
  if (debug_mod) {
    cout << "\033[36meval : \033[37m" << l << endl;
    cout << "\033[32menv : \033[37m" << env << endl;
  }
  if (null(l)) {
    return l;
  }
  if (eq(l, t())) {
    return l;
  }
  if (numberp (l)) {
    return l;
  }
  if (stringp(l)) {
    return l;
  }
  if (symbolp(l)) {
    return find_value(Object_to_symbol(l), env);
  }
  if (symbolp(car(l)) && Object_to_symbol(car(l)) == "quote") {
    return cadr(l);
  }
  if (symbolp(car(l)) && Object_to_symbol(car(l)) == "cond") {
    return eval_cond(cdr(l),env);
  }
  if ((symbolp(car(l)) && Object_to_symbol(car(l)) == "if")) {
    return eval_if(cdr(l),env);
  }
  if ((symbolp(car(l)) && Object_to_symbol(car(l)) == "let")) {

    return eval_let(cadr(l),caddr(l),env);
  }
  if ((symbolp(car(l)) && Object_to_symbol(car(l)) == "lambda")) {
    return l;
  }
  if (listp(l)) {
    return apply(car(l), eval_list(cdr(l),env), env);
  } else {
    throw Evaluation_Exception(l, env, "No_binding");
  }
}

Object apply(const Object& f, const Object& lvals, const Environment& env) {
  if (debug_mod)
  {
    cout << "\033[36mapply :\033[37m" << f << "\033[36mlvals \033[37m" << lvals << endl;
  cout << "\033[32menv:\033[37m" << env << endl;
}
  if (null(f)) {
    throw Evaluation_Exception(f, env, "apply:null");
  }
  if (numberp (f)) {
    throw Evaluation_Exception(f, env, "apply:number");
  }
  if (stringp (f)) {
    throw Evaluation_Exception(f, env, "apply:string");
  }
  if (symbolp(f)) {
    return apply_symbol(f,lvals,env);
  }
  if ((symbolp(car(f)) && Object_to_symbol(car(f)) == "lambda")) {

    return eval(caddr(f), extend_env(cadr(f), lvals, env));
  } else {
    throw Evaluation_Exception(f, env, "apply: cannot apply a list");
  }
}

Object eval_cond(const Object& lclauses, const Environment&env) {
  if (debug_mod)
  {
    cout << "\033[36meval_cond : \033[37m" << lclauses << endl;
  cout << "\033[32menv : \033[37m" << env << endl;
}
  if (null(lclauses)) {
    return nil();
  }
  if (eq(eval(caar(lclauses),env),t())) {
    return eval(cadar(lclauses),env);
  } else {
    return eval_cond(cdr(lclauses),env);
  }
}

Object eval_if(const Object& lvals, const Environment&env) {
  if (debug_mod)
  {
    cout << "\033[36meval_if : \033[37m" << lvals << endl;
  cout << "\033[32menv : \033[37m" << env << endl;
}
  if (eq(eval(car(lvals),env),t())) {
    return eval(cadr(lvals),env);
  } else {
    return eval(caddr(lvals),env);
  }
}

Object eval_let(const Object& lbindings, const Object& body,const Environment&env) {
  return eval(body,extend_env(unzip_left(lbindings),eval_list(unzip_right(lbindings),env),env));
}

Object unzip_left(const Object& lbindings) {
  if (null(lbindings)) {
    return nil();
  } else {
    return cons(caar(lbindings),unzip_left(cdr(lbindings)));
  }
}

Object unzip_right(const Object& lbindings) {
  if (null(lbindings)) {
    return nil();
  } else {
    return cons(cadar(lbindings),unzip_right(cdr(lbindings)));
  }
}

Environment extend_env(const Object& lpars, const Object& lvals, const Environment& env) {
  if (null(lpars)) {
    if (null(lvals)) {
      return env;
    } else {
      throw Zipping_Exception(lpars, "extend_env:too many args");
    }
  } else {
    if (null(lvals)) {
      throw Zipping_Exception(lpars, "extend_env:not enough args");
    } else {
      Environment new_env = copy_env(env);
      add_new_binding(Object_to_symbol(car(lpars)), car(lvals), new_env);
      return extend_env(cdr(lpars), cdr(lpars), new_env);
    }
  }
}

Object eval_list( const Object& lvals, const Environment& env) {
  if (debug_mod)
  {
    cout << "\033[36meval_list : \033[37m" <<  lvals << endl;
  cout << "\033[32menv : \033[37m" << env << endl;
}
  if (null( lvals)) {
    return nil();
  } else {
    return cons( eval(car(lvals),env), eval_list(cdr(lvals),env));
  }
}


Object apply_symbol(const Object& f, const Object& lvals, const Environment& env) {
  if (debug_mod)
  {
    cout << "\033[36mapply_symbol : \033[37m" << f << endl;
  cout << "\033[32menv : \033[37m" << env << endl;
}
  if (symbolp(f) && Object_to_symbol(f) == "+") {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if (numberp(x) && numberp(y)) {
      return number_to_Object(Object_to_number(x) + Object_to_number(y));
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol:not a number");
    }
  }
  if (symbolp(f) && Object_to_symbol(f) == "-") {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if (numberp(x) && numberp(y)) {
      return number_to_Object(Object_to_number(x) - Object_to_number(y));
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol:not a number");
    }
  }
  if (symbolp(f) && Object_to_symbol(f) == "*") {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if (numberp(x) && numberp(y)) {
      return number_to_Object(Object_to_number(x) * Object_to_number(y));
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol:not a number");
    }
  }
  if (symbolp(f) && Object_to_symbol(f) == "=") {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if ((numberp(x) && numberp(y)) || (stringp(x) && stringp(y)) || (listp(x) && listp(y)) || (symbolp(x) && symbolp(y)) || (null(x) && null(y)) || (eq(x,t()) && eq(y,t()))) {
      if (eq(x,y)) {
        return t();
      } else {
        return nil();
      }
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol: = not same type");
    }
  }
  if (symbolp(f) && Object_to_symbol(f) == "car") {
    return car(car(lvals));
  }
  if (symbolp(f) && Object_to_symbol(f) == "cdr") {
    return cdr(car(lvals));
  }
  if (symbolp(f) && Object_to_symbol(f) == "cons") {
    return cons(car(lvals), cadr(lvals));
  }
  if (symbolp(f) && Object_to_symbol(f) == "null") {
    if (null(car(lvals))) {
      return t();
    } else {
      return nil();
    }
  }
  if (symbolp(f) && Object_to_symbol(f) == "end") {
    throw runtime_error("See you later, alLISPator!");
  } else {
    return apply(eval(f,env), lvals, env);
  }
}
