#pragma once

#include <map>
#include <cassert>

#include "interface.hh"

using namespace std;

class Subr {
public:
  typedef Object (*subr_type)(Object);

private:
  typedef map<string, subr_type> table_type;
  table_type table;
  Subr();
  void init();
  static Subr subr_table;

public:
  static void add(string s, subr_type f);
  static bool exists(string s);
  static subr_type get(string s);

};
