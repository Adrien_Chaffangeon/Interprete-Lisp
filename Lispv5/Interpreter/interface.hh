#pragma once

#include <iostream>
#include <cstdio>
#include <exception>
#include <stdexcept>

using namespace std;

/*Define in cell.hh*/
class Cell;

typedef Cell* Object;
extern bool debug_mod;

/*Object nil() : void -> Object
  return : returns Object_nil defined in cell.hh*/
Object nil();

/**
Test if the object is null
\param object to test
\return true if object is null
*/
bool null(const Object& l);

/**
Give Object_t defined in cell.hh
\param nothing
\return Object_t
*/
Object t();



/**
Return Object that is a list which contains a and l
\param a is an Object, l is either Object_nil or an Object wich is a list
\return Object wich is a list containing a and l
*/
Object cons(const Object& a, const Object& l);

/**
Return the first element of l
\param Object wich contains at least one Object
\return the first Object that is in l
*/
Object car(const Object& l);

/**
Return l without its first element
\param Object wich contains at least one Object
\return l without its first element
*/
Object cdr(const Object& l);

/**
Test if a is equal to b
\param two objects that we want to compare
\return true if a is equal to b, false otherwise
*/
bool eq(const Object& a, const Object& b);



/**
Give an Object that contains n
\param an integer
\return an Object that contains n
*/
Object number_to_Object(int n);

/**
Give an Object that contains s
\param a string
\return an Object that contains s
*/
Object string_to_Object(string s);

/**
Give an Object that contains the symbol s
\param a string
\return an Object that contains s
*/
Object symbol_to_Object(string s);

/**
Give an Object that represent a boolean
\param a boolean
\return Object_nil if b is false, Object_t otherwise
*/
Object bool_to_Object(bool b);


/**
Give the value of an Object that contains an integer
\param an Object
\return the integer contained in the Object
*/
int Object_to_number(const Object& l);

/**
Give the value of an Object that contains a string
\param an Object
\return the string contained in the Object
*/
string Object_to_string(const Object& l);

/**
Give the value of an Object that contains a symbol
\param an Object
\return the string associated to the symbol contained in the Object
*/
string Object_to_symbol(const Object& l);


/**
Test if the Object contains a number
\param an object to test
\return true if the Object contains a number, false otherwise
*/
bool numberp(const Object& l);

/**
Test if the Object contains a string
\param an object to test
\return true if the Object contains a string, false otherwise
*/
bool stringp(const Object& l);

/**
Test if the Object contains a symbol
\param an object to test
\return true if the Object contains a symbol, false otherwise
*/
bool symbolp(const Object& l);

/**
Test if the Object contains a list
\param an object to test
\return true if the Object contains a list, false otherwise
*/
bool listp(const Object& l);

/**
Overload << for an Object
*/
ostream& operator << (ostream& s, const Object& l);

/**
The following functions are a shorcut to a sequence of car and cdr. An a reprenst a car, a d reprent a cdr
*/
Object cadr(const Object& l);
Object cdar(const Object& l);
Object caar(const Object& l);
Object cddr(const Object& l);
Object cadar(const Object& l);
Object caddr(const Object& l);
Object cdddr(const Object& l);
Object cadddr(const Object& l);

/****************************************************/

typedef Object Environment;

/**
Exception for an element which is not in an environment
*/
class No_Binding_Exception: public runtime_error {
private:
  string name;
public:
  No_Binding_Exception(string _name);
  virtual ~No_Binding_Exception() throw ();
};

/**
Give a new environment
*/
Environment make_environment();

/**

*/
void add_new_binding(string name, const Object&  value, Environment& env);

Object find_value(string name, const Environment& env);

Environment copy_env(const Environment& env);

ostream& print_env(ostream& s, const Environment& env);

/****************************************************/

class Reader_Exception: public runtime_error {
private:
  string message;
public:
  Reader_Exception(string _message);
  virtual ~Reader_Exception() throw ();
};

Object read_object();

/****************************************************/

class Memory_Overflow_Exception: public runtime_error {
private:
  string message;
public:
  Memory_Overflow_Exception(string _message);
  virtual ~Memory_Overflow_Exception() throw ();
};

void dump();
/****************************************************/

class Continue_Exception: public runtime_error {
private:
  string message;
public:
  Continue_Exception(string _message);
  virtual ~Continue_Exception() throw ();
};

/****************************************************/

class Evaluation_Exception: public runtime_error {
private:
  Object obj;
  Environment env;
  string message;
public:
  Evaluation_Exception(const Object& _obj, const Environment& _env, string _message);
  virtual ~Evaluation_Exception() throw ();
};

class Zipping_Exception: public runtime_error {
private:
  string message;
  Object lobjs;
public:
  Zipping_Exception(const Object& _lobjs, string _message);
  virtual ~Zipping_Exception() throw ();
};
