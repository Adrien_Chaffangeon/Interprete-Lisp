#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <stdexcept>
#include <cassert>

#include "../Interpreter/interface.hh"

using namespace std;

class Token {
public:
  /* Spaces are no token */
  enum token_sort {NIL,LPAR,RPAR,NUMBER,STRING,SYMBOL};

private:
  token_sort tok_sort;
  int number;
  string string_value;
  string symbol_value;

public:
  Token(token_sort tok,int num,string str,string symbol): tok_sort(tok), number(num),string_value(str),symbol_value(symbol) {}

  token_sort get_sort() const;

  string get_value() const;

  int get_number() const;

  string get_symbol() const;
};

class Buffer {
private:
  string line;
  unsigned index;

public:
  Buffer(): line("a"), index(0) {
    getline(cin,line);
    line = ' ' + line;
  }

  unsigned get_index() const {
    return index;
  }

  void set_index(int n) {
    index = n;
  }

  unsigned get_size() const {
    return line.size();
  }

  char look_char() {
    if (index == (line.size()-1)) {
      return (' ');
    } else {
      return line.at(index+1);
    }
  }

  char get_char(bool search_double_quote) {
    if (index == (line.size()-1)) {
      if (search_double_quote) {
        throw Reader_Exception("There is no second double quote");
      }
      index = 0;
      getline(cin,line);
      line = ' ' + line;
      return line.at(index);
    } else {
      index++;
      return line.at(index);
    }
  }
};

Token get_token(Buffer *buff);

Token look_token(Buffer *buff);

Object token_to_obj(Token tok);

Object get_expr(Buffer *buff, int first);

Object get_RPAR(Buffer *buff);
/*
int main();
*/
Object read_object();
