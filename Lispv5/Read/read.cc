#include "read.hh"
#include "../Interpreter/interface.hh"

using namespace std;

Reader_Exception::Reader_Exception(string _message):
  runtime_error(_message) {
  message = _message;
}

Reader_Exception::~Reader_Exception() throw () {}


Token::token_sort Token::get_sort() const {
  return tok_sort;
}

string Token::get_value() const {
  return string_value;
}

int Token::get_number() const {
  return number;
}

string Token::get_symbol() const {
  return symbol_value;
}

/* permet de voir le prochain token situé après index */
Token get_token(Buffer *buff) {
  char a = buff->get_char(false);
  bool cond1 = (a == ' ' || a == '+' || a == '*' || a == '=' || a == '-' || a == '"');
  bool cond2 = (a == '(' || a == ')' || a == '\'');
  bool cond3 = (isdigit(a) || isalpha(a));
  assert(cond1 || cond2 || cond3);
  if (a == ' ') {
    return get_token(buff);
  }
  if (a == '\'') {
    Token tok(Token::SYMBOL,0,"quelconque","quote");
    return tok;
  }
  /* Si c'est une opération */
  if (a == '+' || a == '*' || a == '=') {
    /* Pour ne pas lire deux fois le même token */
    char b[0];
    b[0] = a;
    string symbol = b;
    Token tok(Token::SYMBOL,0,"quelconque",symbol);
    return tok;
  }
  if (a == '-') {
    char next = buff->look_char();
    if (isdigit(next)) {
      char nextint = next;
      int b = nextint;
      while(isdigit(nextint)) {
        b = 10 * b + next;
        nextint = buff->get_char(false);
      }
      Token tok(Token::NUMBER,-b,"quelconque","quelconque");
      return tok;
    } else {
      char b[0];
      b[0] = a;
      string symbol = b;
      Token tok(Token::SYMBOL,0,"quelconque",symbol);
      return tok;
    }
  }
  if (isdigit(a)) {
    /* dû au code Ascii des nombres de 0 à 9 */
    int b = a-48;
    char next = buff->look_char();
    while(isdigit(next)) {
      next = buff->get_char(false);
      b = 10 * b + next-48;
      next = buff->look_char();
    }
    Token tok(Token::NUMBER,b,"quelconque","quelconque");
    return tok;
  }
  if (isalpha(a)) {
    char b[0];
    b[0] = a;
    string s = b;
    char next = buff->look_char();
    while(isalpha(next)) {
      next = buff->get_char(false);
      s = s + next;
      next = buff->look_char();
    }
    Token tok(Token::SYMBOL,0,"quelconque",s);
    return tok;
  }
  if (a == '"') {
    string s = "";
    char next = buff->look_char();
    while(next != '"') {
      next = buff->get_char(true);
      s = s + next;
      next = buff->look_char();
    }
    Token tok(Token::STRING,0,s,"quelconque");
    return tok;
  }
  if (a == '(') {
    char next = buff->look_char();
    if (next == ')') {
      buff->get_char(false);
      Token tok(Token::NIL,0,"quelconque","quelconque");
      return tok;
    } else {
      Token tok(Token::LPAR,0,"quelconque","quelconque");
      return tok;
    }
  }
  if (a == ')') {
    Token tok(Token::RPAR,0,"quelconque","quelconque");
    return tok;
  }
  throw Reader_Exception("illegal entry");
}

Token look_token(Buffer *buff) {
  int save = buff->get_index();
  Token tok = get_token(buff);
  buff->set_index(save);
  return tok;
}

Object token_to_obj(Token tok) {
  if (tok.get_sort() == Token::NUMBER) {
    return (number_to_Object(tok.get_number()));
  }
  if (tok.get_sort() == Token::STRING) {
    return (string_to_Object(tok.get_value()));
  }
  if (tok.get_sort() == Token::SYMBOL) {
    return (symbol_to_Object(tok.get_symbol()));
  }
  if (tok.get_sort() == Token::NIL) {
    return (nil());
  }
  if (tok.get_sort() == Token::RPAR) {
    return (nil());
  }
  throw Reader_Exception("a");
}

Object get_exp(Buffer *buff) {
  Token tok = look_token(buff);
  if (tok.get_sort() == Token::LPAR) {
    return (get_expr(buff,1));
  }
  Object obj = token_to_obj(get_token(buff));
  return (obj);
}

Object get_expr(Buffer *buff,int first) {
  Token tok = get_token(buff);
  if (first == 1) {
    return (get_expr(buff,0));
  }
  if (tok.get_sort() == Token::LPAR) {
    Object obj1 = get_expr(buff,0);
    Object obj2 = get_expr(buff,0);
    return (cons(obj1,obj2));
  }
  if (tok.get_sort() == Token::NIL) {
    Object obj1 = get_expr(buff,0);
    return (cons(nil(),obj1));
  }
  if (tok.get_sort() == Token::RPAR) {
    return (nil());
  }
  Object a = get_expr(buff,0);
  return (cons(token_to_obj(tok),a));
}

Object read_object() {
  Buffer a;
  Object b = get_exp(&a);
  return b;
}

