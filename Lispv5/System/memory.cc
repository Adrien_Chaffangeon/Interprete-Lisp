#include <iostream>
#include <cassert>
#include <vector>
#include <stdexcept>
#include <iomanip>
#include <cstdio>

#include "cell.hh"
#include "memory.hh"

using namespace std;

Memory_Overflow_Exception::Memory_Overflow_Exception(string _message):
  runtime_error(_message){
  message = _message;
}
Memory_Overflow_Exception::~Memory_Overflow_Exception() throw () {}

Memory::Memory(unsigned _size) {
  size = _size;
  contents = vector<Cell>(size);
  for(unsigned i = 0; i < size; i++) {
    contents[i].index = i;
  }
  Cell::init_Object();
}

Memory Memory::global_memory(10000);

void Memory::clear_cell(Cell& c) {
  if (c.sort == Cell::PAIR){
    clear_cell(*c.value.as_pair.item);
    clear_cell(*c.value.as_pair.next);
  }
  global_memory.contents[c.index].sort = Cell::UNDEFINED;
}

Cell* Memory::allocate() {
  unsigned i = 0;
  while(global_memory.contents[i].sort != Cell::UNDEFINED) {
    i++;
    if (i >= global_memory.size) {
      throw Memory_Overflow_Exception("Pas de place");
    }
  }
  return &global_memory.contents[i];
}

void Memory::free_cell(Cell* c) {
  clear_cell(*c);
}


void dump() {
  for(unsigned i = 0; i < Memory::global_memory.size; i++) {
    cout << i << " : " << Memory::global_memory.contents[i] << endl;
  }
}
