#include <cassert>
#include <iostream>
#include <string>

#include "cell.hh"
#include "env.hh"
#include "read.hh"

#include "interface.hh"

using namespace std;

Object nil(){
  return Cell::Object_nil;
}

bool null(const Object& l){
  if(l->is_symbol()){
    if(l->to_string().compare("nil") == 0){
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}

Object t(){
  return Cell::Object_t;
}

Object cons(const Object& a, const Object& l){
  assert(a->is_number() || a->is_string() || a->is_symbol() || a->is_pair());
  assert(l->is_pair() || null(l));
  return Cell::make_cell_pair(a, l);
}

Object car(const Object& l){
  assert(l->is_pair());
  return l->to_pair_item();
}

Object cdr(const Object& l){
  assert(l->is_pair());
  return l->to_pair_next();
}

bool eq(const Object& a, const Object& b){
  if(a->is_number() && b->is_number()){
    return a->to_number() == b->to_number();
  }
  if(a->is_symbol() && b->is_symbol()){
    return a->to_string() == b->to_string();
  }
  if(a->is_string() && b->is_string()){
    return a->to_string() == b->to_string();
  }
  if(a->is_pair() && b->is_pair()){
    return a == b;
  }
  return false;
}

Object number_to_Object(int n){
  return Cell::make_cell_number(n);
}

Object string_to_Object(string s){
  return Cell::make_cell_string(s);
}

Object symbol_to_Object(string s){
  return Cell::make_cell_symbol(s);
}

Object bool_to_Object(bool b){
  if(b){
    return t();
  }else{
    return nil();
  }
}

int Object_to_number(const Object& l){
  assert(l->is_number());
  return l->to_number();
}

string Object_to_string(const Object& l){
  assert(l->is_string());
  return l->to_string();
}

string Object_to_symbol(const Object& l){
  assert(l->is_symbol());
  return l->to_string();
}

bool numberp(const Object& l){
  return l->is_number();
}

bool stringp(const Object& l){
  return l->is_string();
}

bool symbolp(const Object& l){
  return l->is_symbol();
}

bool listp(const Object& l){
  return l->is_pair();
}

ostream& operator << (ostream& s, const Object& l){
  return s << *l;
}

Object cadr(const Object& l){
  //assert déjà dans car et cdr
  return car(cdr(l));
}

Object cdar(const Object& l){
  return cdr(car(l));
}

Object caar(const Object& l){
  return car(car(l));
}

Object cddr(const Object& l){
  return cdr(cdr(l));
}

Object cadar(const Object& l){
  return car(cdr(car(l)));
}

Object caddr(const Object& l){
  return car(cddr(l));
}

Object cdddr(const Object& l){
  return cdr(cddr(l));
}

Object cadddr(const Object& l){
  return car(cdddr(l));
}




