#pragma once

#include <iostream>
#include <vector>

#include "memory.hh"

#include "interface.hh"

using namespace std;

class Memory;

class Cell {
private:
  enum cell_sort {UNDEFINED, NUMBER, STRING, SYMBOL, PAIR};
  cell_sort sort;

  struct cell_pair {
    Cell* item;
    Cell* next;
  };

  union cell_value {
    int as_number;
    char *as_string;
    char *as_symbol;
    cell_pair as_pair;
  };

  cell_value value;

  unsigned index;

private:
  Cell(); // No direct allocation
  static string sort_to_string(cell_sort k);

public:
  static Cell* Object_nil; //make_cell_symbol("nil")
  static Cell* Object_t; //make_cell_symbol("t")

public:
  void check() const;
  cell_sort get_sort() const;
  bool is_number() const;
  bool is_string() const;
  bool is_symbol() const;
  bool is_pair() const;

  int to_number() const;
  string to_string() const;
  Cell* to_pair_item() const;
  Cell* to_pair_next() const;

  static Cell* make_cell_number(int a);
  static Cell* make_cell_string(string s);
  static Cell* make_cell_symbol(string s);
  static Cell* make_cell_pair(Cell* const p, Cell* const q);

  static void init_Object();

  friend ostream& operator <<(ostream& s, const Cell& c);
};

ostream& operator <<(ostream& s, const Cell& c);
