#include "read.hh"
#include "../Interpreter/interface.hh"

using namespace std;

/* permet de voir le prochain token situé après index */
Token get_token(Buffer *buff){
	char a = buff->get_char();
	assert(a != '&');
	if (a == ' '){
		return get_token(buff);
	}
	/* Si c'est une opération */
	if (a == '+' || a == '*' || a == '='){
		/* Pour ne pas lire deux fois le même token */
		char b[0];
		b[0] = a;
		string symbol = b;
		Token tok(Token::SYMBOL,0,"quelconque",symbol);
		return tok;
	}
	if (a == '-'){
		char next = buff->look_char();
		if (isdigit(next)){
			char nextint = next;
			int b = nextint;
			while(isdigit(nextint)){
				b = 10 * b + next;
				nextint = buff->get_char();
			}
			Token tok(Token::NUMBER,-b,"quelconque","quelconque");
			return tok;
		}
		else{
			char b[0];
			b[0] = a;
			string symbol = b;
			Token tok(Token::SYMBOL,0,"quelconque",symbol);
			return tok;
		}
	}
	if (isdigit(a)){
		/* dû au code Ascii des nombres de 0 à 9 */
		int b = a-48;
		char next = buff->get_char();
		while(isdigit(next)){
			b = 10 * b + next-48;
			next = buff->get_char();
		}
		Token tok(Token::NUMBER,b,"quelconque","quelconque");
		return tok;
	}
	if (isalpha(a)){
		char b[0];
		b[0] = a;
		string s = b;
		char next = buff->get_char();
		while(isalpha(next)){
			s = s + next;
			next = buff->get_char();
		}
		Token tok(Token::NUMBER,0,s,"quelconque");
		return tok;
	}
	if (a == '('){
		char next = buff->look_char();
		if (next == ')'){
			Token tok(Token::NIL,0,"quelconque","quelconque");
			return tok;
		}
		else{
			Token tok(Token::LPAR,0,"quelconque","quelconque");
			return tok;
		}
	}
	if (a == ')'){
		Token tok(Token::RPAR,0,"quelconque","quelconque");
		return tok;
	}
	/*assert(0)*/
}

Token look_token(Buffer *buff){
	int save = buff->get_index();
	Token tok = get_token(buff);
	buff->set_index(save);
	return tok;
}

Object token_to_obj(Token tok){
	if (tok.get_sort() == Token::NUMBER){
		return (number_to_Object(tok.get_number()));
	}
	if (tok.get_sort() == Token::STRING){
		return (string_to_Object(tok.get_value()));
	}
	if (tok.get_sort() == Token::SYMBOL){
		return (symbol_to_Object(tok.get_symbol()));
	}
	if (tok.get_sort() == Token::NIL){
		return (nil());
	}
	if (tok.get_sort() == Token::RPAR){
		return (nil());
	}
}

Object get_expr(Buffer *buff){
	Token tok = get_token(buff);
	if (tok.get_sort() == Token::LPAR){
		return get_RPAR(buff);
	}
	else{
		return (token_to_obj(tok));
	}
}

Object get_RPAR(Buffer *buff){
	Token tok = look_token(buff);
	if (tok.get_sort() == Token::LPAR){
		Object obj = get_expr(buff);
		return cons(obj,get_RPAR(buff));
	}
	tok = get_token(buff);
	if (tok.get_sort() == Token::NIL){
		Object obj = get_RPAR(buff);
		return cons(nil(),get_RPAR(buff));
	}
	if (tok.get_sort() == Token::RPAR){
		return (nil());
	}
	Object obj = token_to_obj(tok);
	return cons(obj,get_RPAR(buff));
}
/*
int main(){
	Buffer a;
	Token tok1 = get_token(&a);
	Token tok2 = get_token(&a);
	Token tok3 = get_token(&a);
	cout << tok1.get_symbol() << endl;
	cout << tok2.get_number() << endl;
	cout << tok3.get_symbol() << endl;
	return 0;
}
*/

Object read_object(){
	static int counter = 0;
	Object n = number_to_Object(counter);
	Object m = number_to_Object(counter+1);
	Object plus = symbol_to_Object("+");
	Object op = cons(plus, cons(n,cons(m,nil())));
	
	counter++;
	return op;
	}
