#include <iostream>

#include "eval.hh"
#include "toplevel.hh"

#include "interface.hh"
#include "read.hh"
#include "env.hh"

using namespace std;

Continue_Exception::Continue_Exception(string _message):
  runtime_error(_message) {
  message = _message;
}

Continue_Exception::~Continue_Exception() throw () {}

void toplevel(bool use_prompt, Environment& env) {
  int compteur = 0;
  while (compteur <1) {
    cout <<endl<< "Lisp? " << flush;
    /*Object obj1 = cons(symbol_to_Object("-"), 
                       cons(number_to_Object(2),
                            cons(number_to_Object(3), nil())));
    Object obj2 = cons(symbol_to_Object("+"), 
                      cons(number_to_Object(1), 
                           cons(number_to_Object(2), nil())));
    Object obj = cons(symbol_to_Object("="), 
                      cons(obj1, 
                           cons( obj2, nil())));*/
    /*Object obj = number_to_Object(2);*/
    /*Object obj = read_object();*/
    add_new_binding("a", number_to_Object(1), env);
    add_new_binding("a", number_to_Object(2), env);
    add_new_binding("b", number_to_Object(3), env);
    Object obj = cons(symbol_to_Object("+"), 
                      cons(symbol_to_Object("a"), 
                           cons( symbol_to_Object("b"), nil())));
    
    try {
      compteur++;
      cout << "#?>" << eval(obj, env) << endl;

    } catch (No_Binding_Exception& e) {
      clog << e.what() << endl;

    } catch (Continue_Exception e) {
      clog << e.what() << endl;

    } catch (Evaluation_Exception e) {
      clog << e.what() << endl;

    } catch (Zipping_Exception e) {
      clog << e.what() << endl;
    }
  }
}
