#pragma once

#include <iostream>
#include <cstdio>
#include <exception>
#include <stdexcept>

using namespace std;

class Cell;

typedef Cell* Object;

Object nil();
bool null(const Object& l);
Object t();

Object cons(const Object& a, const Object& l);
Object car(const Object& l);
Object cdr(const Object& l);
bool eq(const Object& a, const Object& b);

Object number_to_Object(int n);
Object string_to_Object(string s);
Object symbol_to_Object(string s);
Object bool_to_Object(bool b);

int Object_to_number(const Object& l);
string Object_to_string(const Object& l);
string Object_to_symbol(const Object& l);

bool numberp(const Object& l);
bool stringp(const Object& l);
bool symbolp(const Object& l);
bool listp(const Object& l);

ostream& operator << (ostream& s, const Object& l);

Object cadr(const Object& l);
Object cdar(const Object& l);
Object caar(const Object& l);
Object cddr(const Object& l);
Object cadar(const Object& l);
Object caddr(const Object& l);
Object cdddr(const Object& l);
Object cadddr(const Object& l);

/****************************************************/

typedef Object Environment;

class No_Binding_Exception: public runtime_error {
private:
  string name;
public:
  No_Binding_Exception(string _name);
  virtual ~No_Binding_Exception() throw ();
};

Environment make_environment();

void add_new_binding(string name, const Object&  value, Environment& env);

Object find_value(string name, const Environment& env);

Environment copy_env(const Environment& env);

ostream& print_env(ostream& s, const Environment& env);

/****************************************************/
/*
class Reader_Exception: public runtime_error {
private:
  string message;
public:
  Reader_Exception(string _message);
  virtual ~Reader_Exception() throw ();
};

Object read_object();
*/
/****************************************************/
/*
class Memory_Overflow_Exception: public runtime_error {
private:
  string message;
public:
  Memory_Overflow_Exception(string _message);
  virtual ~Memory_Overflow_Exception() throw ();
};
*/
/****************************************************/

class Continue_Exception: public runtime_error {
private:
  string message;
public:
  Continue_Exception(string _message);
  virtual ~Continue_Exception() throw ();
};

/****************************************************/

class Evaluation_Exception: public runtime_error {
private:
  Object obj;
  Environment env;
  string message;
public:
  Evaluation_Exception(const Object& _obj, const Environment& _env, string _message);
  virtual ~Evaluation_Exception() throw ();
};

class Zipping_Exception: public runtime_error {
private:
  string message;
  Object lobjs;
public:
  Zipping_Exception(const Object& _lobjs, string _message);
  virtual ~Zipping_Exception() throw ();
};
