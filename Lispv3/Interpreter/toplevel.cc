#include <iostream>

#include "eval.hh"
#include "toplevel.hh"

#include "interface.hh"
#include "read.hh"
#include "env.hh"

using namespace std;

Continue_Exception::Continue_Exception(string _message):
  runtime_error(_message) {
  message = _message;
}

Continue_Exception::~Continue_Exception() throw () {}

void toplevel(bool use_prompt, Environment& env) {
  while (true) {
    cout <<endl<< "\033[34mLisp? \033[37m" << flush;
    //Object para = cons( symbol_to_Object("n"), nil());
    //Object op = cons( symbol_to_Object("+"), cons( symbol_to_Object("n"), cons(number_to_Object(1), nil())));
    //Object f = cons(symbol_to_Object("lambda"), cons( para, cons(op, nil())));
    //Object n = cons( symbol_to_Object("n"), cons(number_to_Object(2), nil()));
    // Object obj = cons(symbol_to_Object("cond"), cons( cons(nil(), cons(number_to_Object(4), nil())), cons(cons(t(), cons(number_to_Object(5),nil())), nil())));
    // Object obj = cons(symbol_to_Object("null"), cons( cons( symbol_to_Object("quote"),cons( number_to_Object(2), nil())), nil()));
    //"lambda" :: Object obj = cons(f,cons(number_to_Object(2),nil())); 
    // Object obj = cons(symbol_to_Object("let"), cons(cons(n,nil()),cons(op,nil())));
    // Object obj = cons(symbol_to_Object("cons"), cons(number_to_Object(2), cons(nil() ,nil())));
    Object obj = read_object();
    //Object obj = cons(symbol_to_Object("+"), cons(number_to_Object(1), cons(number_to_Object(2), nil())));                      
    try {
      cout << "\033[31m#?>" << eval(obj, env) << endl;

    } catch (No_Binding_Exception& e) {
      clog << e.what() << endl;

    } catch (Continue_Exception e) {
      clog << e.what() << endl;

    } catch (Evaluation_Exception e) {
      clog << e.what() << endl;

    } catch (Zipping_Exception e) {
      clog << e.what() << endl;
    }
  }
}
