#include <cassert>

#include "subr.hh"
#include "fsubr.hh"
#include "eval.hh"

#include "interface.hh"

using namespace std;

Evaluation_Exception::Evaluation_Exception(const Object& _obj, const Environment& _env, string _message):
  runtime_error(_message) {
  message = _message;
}

Evaluation_Exception::~Evaluation_Exception() throw () {}

Zipping_Exception::Zipping_Exception(const Object& _lobjs, string _message):
  runtime_error(_message) {
  message = _message;
}

Zipping_Exception::~Zipping_Exception() throw () {}

Object eval(const Object& l, const Environment& env) {
  cout << "eval :" << l << endl;
  if (null(l)) {
    return l;
  }
  if (eq(l, t())) {
    return l;
  }
  if (numberp (l)) {
    return l;
  }
  if (stringp(l)) {
    return l;
  }
  if (symbolp(l)) {
    return find_value(Object_to_symbol(l), env);
  }
  if (eq(car(l),symbol_to_Object("quote"))) {
    return cadr(l);
  }
  if (eq(car(l),symbol_to_Object("cond"))) {
    return eval_cond(cdr(l),env);
  }
  if (eq(car(l),symbol_to_Object("if"))) {
    return eval_if(cdr(l),env);
  }
  if (eq(car(l),symbol_to_Object("let"))) {

    return eval_let(cadr(l),caddr(l),env);
  }
  if (eq(car(l),symbol_to_Object("lambda"))) {
    return l;
  }
  if (listp(l)) {
    return apply(car(l), eval_list(cdr(l),env), env);
  } else {
    throw Evaluation_Exception(l, env, "No_binding");
  }
}

Object apply(const Object& f, const Object& lvals, const Environment& env) {
  cout << "apply :" << f << " lvals " << lvals << endl;
  if (null(f)) {
    throw Evaluation_Exception(f, env, "apply:null");
  }
  if (numberp (f)) {
    throw Evaluation_Exception(f, env, "apply:number");
  }
  if (stringp (f)) {
    throw Evaluation_Exception(f, env, "apply:string");
  }
  if (symbolp(f)) {
    return apply_symbol(f,lvals,env);
  }
  if (eq(car(f),symbol_to_Object("lambda"))) {

    return eval(caddr(f), extend_env(cadr(f), lvals, env));
  } else {
    throw Evaluation_Exception(f, env, "apply: cannot apply a list");
  }
}

Object eval_cond(const Object& lclauses, const Environment&env) {
  if (null(lclauses)) {
    return nil();
  }
  if (eq(eval(caar(lclauses),env),t())) {
    return eval(cadar(lclauses),env);
  } else {
    return eval_cond(cdr(lclauses),env);
  }
}

Object eval_if(const Object& lvals, const Environment&env) {
  if (eq(eval(car(lvals),env),t())) {
    return eval(cadr(lvals),env);
  } else {
    return eval(caddr(lvals),env);
  }
}

Object eval_let(const Object& lbindings, const Object& body,const Environment&env) 
{
	return eval(body,extend_env(unzip_left(lbindings),eval_list(unzip_right(lbindings),env),env));
}

Object unzip_left(const Object& lbindings) {
  if (null(lbindings)) {
    return nil();
  } else {
    return cons(caar(lbindings),unzip_left(cdr(lbindings)));
  }
}

Object unzip_right(const Object& lbindings) {
  if (null(lbindings)) {
    return nil();
  } else {
    return cons(cadar(lbindings),unzip_right(cdr(lbindings)));
  }
}

Environment extend_env(const Object& lpars, const Object& lvals, const Environment& env) {
  if (null(lpars)) {
    if (null(lvals)) {
      return env;
    } else {
      throw Zipping_Exception(lpars, "extend_env:too many args");
    }
  } else {
    if (null(lvals)) {
      throw Zipping_Exception(lpars, "extend_env:not enough args");
    } else {
      Environment new_env = copy_env(env);
      add_new_binding(Object_to_symbol(car(lpars)), car(lvals), new_env);
      return extend_env(cdr(lpars), cdr(lpars), new_env);
    }
  }
}

Object eval_list( const Object& lvals, const Environment& env) {
  cout << "eval_list :" <<  lvals << endl;
  if (null( lvals)) {
    return nil();
  } else {
    return cons( eval(car(lvals),env), eval_list(cdr(lvals),env));
  }
}


Object apply_symbol(const Object& f, const Object& lvals, const Environment& env) {
  cout << "apply_symbol :" << f << endl;
  if (eq(f,symbol_to_Object("+"))) {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if (numberp(x) && numberp(y)) {
      return number_to_Object(Object_to_number(x) + Object_to_number(y));
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol:not a number");
    }
  }
  if (eq(f,symbol_to_Object("-"))) {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if (numberp(x) && numberp(y)) {
      return number_to_Object(Object_to_number(x) - Object_to_number(y));
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol:not a number");
    }
  }
  if (eq(f,symbol_to_Object("*"))) {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if (numberp(x) && numberp(y)) {
      return number_to_Object(Object_to_number(x) * Object_to_number(y));
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol:not a number");
    }
  }
  if (eq(f,symbol_to_Object("="))) {
    Object x = car(lvals);
    Object y = cadr(lvals);
    if ((numberp(x) && numberp(y)) || (stringp(x) && stringp(y)) || (listp(x) && listp(y)) || (symbolp(x) && symbolp(y)) || (null(x) && null(y)) || (eq(x,t()) && eq(y,t()))) {
      if (eq(x,y)) {
        return t();
      } else {
        return nil();
      }
    } else {
      throw Evaluation_Exception(f, env, "apply_symbol: = not same type");
    }
  }
  if (eq(f,symbol_to_Object("car"))) {
    cout << "apply_symbol car :" << lvals << endl;
    return car(car(lvals));
  }
  if (eq(f,symbol_to_Object("cdr"))) {
    return cdr(car(lvals));
  }
  if (eq(f,symbol_to_Object("cons"))) {
    return cons(car(lvals), cadr(lvals));
  }
  if (eq(f,symbol_to_Object("null"))) {
    if (null(car(lvals))) {
      return t();
    } else {
      return nil();
    }
  }
  if (eq(f,symbol_to_Object("end"))) {
    throw Evaluation_Exception(f, env, "See you later, alLISPator!");
  } else {
    return apply(eval(f,env), lvals, env);
  }
}
