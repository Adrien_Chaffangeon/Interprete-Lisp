#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <stdexcept>
#include <cassert>

#include "../Interpreter/interface.hh"

using namespace std;

class Token {
	public:
		/* Spaces are no token */
		enum token_sort {NIL,LPAR,RPAR,NUMBER,STRING,SYMBOL};
		
		Token(token_sort tok,int num,string str,string symbol): tok_sort(tok), number(num),string_value(str),symbol_value(symbol) {}
		
		token_sort get_sort(){
			return tok_sort;
		}
		
		string get_value(){
			return string_value;
		}
		
		int get_number(){
			return number;
		}
		
		string get_symbol(){
			return symbol_value;
		}
		
	private:
		token_sort tok_sort;
		int number;
		string string_value;
		string symbol_value;
};

class Buffer {
	private:
		string line;
		unsigned index ;
	
	public:
		Buffer(): line("a"), index(0) {
			getline(cin,line);
			line = ' ' + line;
		}
		
		unsigned get_index(){
			return index;
		}
		
		void set_index(int n){
			index = n;
		}
		
		unsigned get_size(){
			return line.size();
		}
		
		char look_char(){
			if (index == (line.size()-1)){
				return (')');
			}
			else{
				return line.at(index+1);
			}
		}
		
		char get_char(){
			if (index == (line.size()-1)){
				cout << "fin" << endl;
				return (')');
			}
			else{
				index++;
				cout << index << endl;
				cout << line.at(index) << endl;
				return line.at(index);
			}
		}
};

Token get_token(Buffer *buff);

Token look_token(Buffer *buff);

Object token_to_obj(Token tok);

Object get_expr(Buffer *buff);

Object get_RPAR(Buffer *buff);
/*
int main();
*/
Object read_object();
