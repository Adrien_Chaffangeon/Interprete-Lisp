/* Flex syntax */

BLANK [ \t\n]
LINE [^\n]*\n
ALPHA [a-zA-Z]
OPERATION [-+*/.=_?><]
NUM [0-9]
NIL ((nil)|(NIL)|(Nil))
SYMBOL ({OPERATION}|({ALPHA}({ALPHA}|{NUM})*))
NUMBER (-?{NUM}+)
LPAR \(
RPAR \)
QUOTE \'
STRING \"[^\"]*\"
COMMENT ;{LINE}
