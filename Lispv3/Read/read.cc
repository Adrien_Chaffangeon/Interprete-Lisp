#include "read.hh"
#include "../Interpreter/interface.hh"

using namespace std;

Reader_Exception::Reader_Exception(string _message):
  runtime_error(_message) {
  message = _message;
}

Reader_Exception::~Reader_Exception() throw () {}

/* permet de voir le prochain token situé après index */
Token get_token(Buffer *buff){
	char a = buff->get_char();
	/*assert(a != '&');*/
	if (a == ' '){
		return get_token(buff);
	}
	/* Si c'est une opération */
	if (a == '+' || a == '*' || a == '='){
		/* Pour ne pas lire deux fois le même token */
		char b[0];
		b[0] = a;
		string symbol = b;
		Token tok(Token::SYMBOL,0,"quelconque",symbol);
		return tok;
	}
	if (a == '-'){
		char next = buff->look_char();
		if (isdigit(next)){
			char nextint = next;
			int b = nextint;
			while(isdigit(nextint)){
				b = 10 * b + next;
				nextint = buff->get_char();
			}
			Token tok(Token::NUMBER,-b,"quelconque","quelconque");
			return tok;
		}
		else{
			char b[0];
			b[0] = a;
			string symbol = b;
			Token tok(Token::SYMBOL,0,"quelconque",symbol);
			return tok;
		}
	}
	if (isdigit(a)){
		/* dû au code Ascii des nombres de 0 à 9 */
		int b = a-48;
		char next = buff->look_char();
		while(isdigit(next)){
			next = buff->get_char();
			b = 10 * b + next-48;
		}
		Token tok(Token::NUMBER,b,"quelconque","quelconque");
		return tok;
	}
	if (isalpha(a)){
		char b[0];
		b[0] = a;
		string s = b;
		char next = buff->get_char();
		while(isalpha(next)){
			s = s + next;
			next = buff->get_char();
		}
		Token tok(Token::NUMBER,0,s,"quelconque");
		return tok;
	}
	if (a == '('){
		char next = buff->look_char();
		if (next == ')'){
			Token tok(Token::NIL,0,"quelconque","quelconque");
			return tok;
		}
		else{
			Token tok(Token::LPAR,0,"quelconque","quelconque");
			return tok;
		}
	}
	if (a == ')'){
		Token tok(Token::RPAR,0,"quelconque","quelconque");
		return tok;
	}
	throw Reader_Exception("illegal entry");
}

Token look_token(Buffer *buff) {
  int save = buff->get_index();
  Token tok = get_token(buff);
  buff->set_index(save);
  return tok;
}

Object token_to_obj(Token tok) {
  if (tok.get_sort() == Token::NUMBER) {
    return (number_to_Object(tok.get_number()));
  }
  if (tok.get_sort() == Token::STRING) {
    return (string_to_Object(tok.get_value()));
  }
  if (tok.get_sort() == Token::SYMBOL) {
    return (symbol_to_Object(tok.get_symbol()));
  }
  if (tok.get_sort() == Token::NIL) {
    return (nil());
  }
  if (tok.get_sort() == Token::RPAR) {
    return (nil());
  }
}

Object get_exp(Buffer *buff){
	Token tok = look_token(buff);
	/*cout << "token" << tok.get_sort() << endl;*/
	if (tok.get_sort() == Token::LPAR){
		return (car(get_expr(buff)));
	}
	Object obj = token_to_obj(get_token(buff));
	/*cout << "mon objet " << obj << endl;*/
	return (obj);
}

Object get_expr(Buffer *buff){
	Token tok = get_token(buff);
	if (tok.get_sort() == Token::LPAR){
		Object obj1 = get_expr(buff);
		Object obj2 = get_expr(buff);
		Object obj3 = get_expr(buff);
		return cons(obj1,cons(obj2,obj3));
	}
	if (tok.get_sort() == Token::RPAR){
		return (nil());
	}
	return (cons(token_to_obj(tok),get_expr(buff)));
}
/*
Object get_RPAR(Buffer *buff){
	Token tok = look_token(buff);
	
	if (tok.get_sort() == Token::LPAR){
		Object obj = get_RPAR(buff);
		return (obj);
	}
	
	else{
	if (tok.get_sort() == Token::RPAR){
		cout << "nil" << endl;
		return (nil());
	}
	
	Object obj = token_to_obj(tok);
	return cons(obj,get_RPAR(buff));
}
*/
Object read_object(){
	Buffer a;
	Object b = get_exp(&a);
	/*cout << "Voici l'objet " << b << endl;*/
	return b;
}
	/*
	static int counter = 0;
	Object n = number_to_Object(counter);
	Object m = number_to_Object(counter+1);
	Object plus = symbol_to_Object("+");
	Object op = cons(plus, cons(n,cons(m,nil())));
	
	counter++;
	return op;
	*/

/*
int main(){
	Buffer a;
	Token tok1 = get_token(&a);
	Token tok2 = get_token(&a);
	Token tok3 = get_token(&a);
	cout << tok1.get_symbol() << endl;
	cout << tok2.get_number() << endl;
	cout << tok3.get_symbol() << endl;
	return 0;
}
*/
